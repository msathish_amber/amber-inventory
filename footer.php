<footer class="page-footer footer footer-static footer-dark gradient-45deg-light-blue-cyan gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; 2019          <a href="https://amberconnect.com" target="_blank">Amber Connect</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a target="_blank" href="https://amberconnect.com">Amber Connect</a></span></div>
    </div>
</footer>

 <script src="vendors/sweetalert/sweetalert.min.js"></script>