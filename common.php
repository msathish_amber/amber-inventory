<div class="preloader-wrapper small " style="position: fixed;top: 50%;left: 50%;">
    <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>

    <div class="spinner-layer spinner-red">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>

    <div class="spinner-layer spinner-yellow">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>

    <div class="spinner-layer spinner-green">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>
</div>


//Alerts Section

<div class="col s12 m4 l4">
    <p>
        <strong>GRADIENT BG WITH ICON</strong>
    </p>
    <p>Add materialize icons class.</p>
    <div class="card-alert card gradient-45deg-purple-deep-orange">
        <div class="card-content white-text">
            <p>
                <i class="material-icons">notifications</i> NEWS : You have done 5 actions.</p>
        </div>
    </div>
    <div class="card-alert card gradient-45deg-light-blue-cyan">
        <div class="card-content white-text">
            <p>
                <i class="material-icons">info_outline</i> INFO : You have 18 messages</p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="card-alert card gradient-45deg-green-teal">
        <div class="card-content white-text">
            <p>
                <i class="material-icons">check</i> SUCCESS : The page has been added.</p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="card-alert card gradient-45deg-red-pink">
        <div class="card-content white-text">
            <p>
                <i class="material-icons">error</i> DANGER : The daily report has failed</p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="card-alert card gradient-45deg-amber-amber" id="error">
        <div class="card-content white-text">
            <p>
                <i class="material-icons">warning</i> <span class="errormsg"></span></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>