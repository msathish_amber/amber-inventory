<?php
include("config/config.php");
if (empty($_SESSION['Amber_Inventory_UserToken']) && $_SESSION['Amber_Inventory_Screen_UserView'] != 'Y') {
    header("Location: index" . $extension);
}
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
        <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>Amber-Inventory</title>
        <link rel="amber-icon" href="images/favicon/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- BEGIN: VENDOR CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/flag-icon/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">

        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/data-tables.css">
        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
        <!--END: Custom CSS-->
        <style>
            .select-wrapper>ul{
                max-height: 200px !important;
            }
            #divFilter span.select2.select2-container.select2-container--default{
                padding-top:10px;
            }
            #divFilter span.select2-selection__arrow{
                top:10px;
            }
        </style>
    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        <!-- BEGIN: Header-->
        <?php include("header.php"); ?>
        <!-- END: Header-->

        <!-- BEGIN: Page Main-->
        <div id="main">
            <div class="row">
                <div class="col s12">
                    <div class="container">

                        <div class="row">
                            <div class="overlay">
                            </div>
                            <div class="col s12 m6 l6">
                                <h5 class="breadcrumbs-title">Users List</h5>

                            </div>
                            <div class="col s12 m6 l6 right-align-md" style="margin-top: 10px;">
                                <?php if ($_SESSION['Amber_Inventory_Screen_UserCreate'] == 'Y') { ?>
                                    <a href="newuser<?php echo $extension; ?>" class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mr-1 mb-1">Create New User</a>
                                <?php } ?>

                            </div>
                            <div class="col s12 m12 l12">
                                <div id="button-trigger" class="card card card-default scrollspy " >
                                    <div class="card-content">
                                        <div class="card-alert card gradient-45deg-green-teal hide" id="success">
                                            <div class="card-content white-text">
                                                <p>
                                                    <i class="material-icons">check</i> <span class="successmsg"></span></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="card-alert card gradient-45deg-red-pink hide" id="error">
                                            <div class="card-content white-text">
                                                <p>
                                                    <i class="material-icons">error</i> <span class="errormsg"></span></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>

                                        <div class="row">

                                            <div class="col s12">
                                                <div class="table-responsive" style="overflow: auto;">
                                                    <table id="tblUser"  class="display nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Email-Id</th>
                                                                <th>User Name</th>
                                                                <th>Groups</th>                                                               
                                                                <th>Action</th>      
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tblbodyStock">

                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->

        <!-- BEGIN: Footer-->
        <?php include("footer.php"); ?>
        <!-- END: Footer-->
        <input type="hidden" id="hdnUserToken" />
        <!-- Modal Structure -->
        <div id="deleteModel" class="modal" style="width:20%;">

            <div class="modal-content">
                <h5>Information</h5>
                <p>Are you sure you want to delete this user?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat ">No</a>
                <a href="javascript:;" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="DeleteUser()">Yes</a>
            </div>
        </div>
        <div id="passwordModel" class="modal" style="width:20%;">
            <input type="hidden" id="hdnUserToken" />
            <div class="modal-content">
                <h5>Reset Password</h5>
                <div >
                    <br>

                    <div class="col s12">
                        <div class="input-field col s12">
                            <input id="password" type="password" class="validate">
                            <label for="password" style="left:0;">New Password</label>
                        </div>
                    </div>
                    <div class="col s12">
                        <div class="input-field col s12">
                            <input id="cpassword" type="password" class="validate">
                            <label for="cpassword" style="left:0;">Confirm Password</label>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
                <a href="javascript:;" class="modal-action  waves-effect waves-green btn-flat " onclick="ChangePassword()">Confirm</a>
            </div>
            <input type="hidden"  id='hdnAdminFlag' value="<?php echo $_SESSION['Amber_Inventory_AdminFlag']; ?>" />
            <input type="hidden"  id='hdnEdit' value="<?php echo $_SESSION['Amber_Inventory_Screen_UserEdit']; ?>" />
            <input type="hidden"  id='hdnDelete' value="<?php echo $_SESSION['Amber_Inventory_Screen_UserDelete']; ?>" />
            <input type="hidden"  id='hdnReset' value="<?php echo $_SESSION['Amber_Inventory_Screen_UserReset']; ?>" />
             <input type="hidden"  id='hdnLoginUserToken' value="<?php echo $_SESSION['Amber_Inventory_UserToken']; ?>" />
        </div>
        <!-- END: Footer-->
        <!-- BEGIN VENDOR JS-->
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script src="vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>
        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="js/scripts/data-tables.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->

        <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jszip.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/FileSaver.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/myexcel.js?version=<?php echo $jsVersion; ?>"></script>
        <script src="js/scripts/ui-alerts.js" type="text/javascript"></script>
        <script>
                    $(function () {
                        $('.modal').modal();
                    });
                    function loadTable() {
                        $('#tblUser').DataTable({
                            dom: 'Bfrtip',
                            "processing": true,
                            "serverSide": true,
                            "bLengthChange": false,

                            "searching": false,
                            "ordering": false,
                            ajax: {
                                url: "ajax/users" + extension,
                                cache: false,
                                type: "POST",
                                data: {
                                    offset: offset,
                                },
                            },
                            columns: [
                                {data: "Name"},
                                {data: "Email"},
                                {data: "UserName"},
                                {data: "GroupName"},
                                {
                                    "mData": null,
                                    "bSortable": false,
                                    "mRender": function (data, type, full) {
                                        var edit = $('#hdnEdit').val();
                                        if (edit != 'Y') {
                                            edit = 'hide';
                                        }
                                        var deleteuser = $('#hdnDelete').val();
                                        if (deleteuser != 'Y') {
                                            deleteuser = 'hide';
                                        }
                                        var reset = $('#hdnReset').val();
                                        if (reset != 'Y') {
                                            reset = 'hide';
                                        }
                                      console.log(data);
                                        var Loginuser = $('#hdnLoginUserToken').val();
                                        if (data.CSRUserToken != Loginuser  && data.UserName != 'amberadmin') {
                                            return "<a class='edittable " + edit + "' style='cursor:pointer; '><i class='material-icons'>edit</i></a>&nbsp;&nbsp;<a class='deletetable " + deleteuser + "'  style='cursor:pointer;'><i style='color:red;' class='material-icons'>delete</i></a>&nbsp;&nbsp;<a class='passwordtable " + reset + "'  style='cursor:pointer;'> <i style='color:orange; ' class='material-icons'>vpn_key</i></a>";
                                        } else {
                                            return 'N/A';
                                        }
                                    }
                                }
                            ],
                        });
                    }
                    loadTable();
                    $('#tblUser tbody').on('click', '.deletetable', function () {
                        var table = $('#tblUser').DataTable();
                        var data = table.row($(this).parents('tr')).data();
                        var UserToken = data["CSRUserToken"];
                        $('#hdnUserToken').val(UserToken);
                        $('#deleteModel').modal('open');
                    });
                    $('#tblUser tbody').on('click', '.passwordtable', function () {
                        var table = $('#tblUser').DataTable();
                        var data = table.row($(this).parents('tr')).data();
                        var UserToken = data["CSRUserToken"];
                        $('#password').val('');
                        $('#cpassword').val('');
                        $('#hdnUserToken').val(UserToken);
                        $('#passwordModel').modal('open');
                    });
                    $('#tblUser tbody').on('click', '.edittable', function () {
                        var table = $('#tblUser').DataTable();
                        var data = table.row($(this).parents('tr')).data();
                        var UserToken = data["CSRUserToken"];
                        var Name = data["Name"];
                        var UserName = data["UserName"];
                        var Email = data["Email"];
                        var GroupId = data["GroupId"];
                        var Screens = data["Screens"];
                        var GroupAdminFlag = data["GroupAdminFlag"];
                        var SuperAdminFlag = data["SuperAdminFlag"];
                        var AdminFlag = data["AdminFlag"];
                        $.ajax({
                            type: "POST",
                            url: "ajax/EditUser" + extension,
                            data: {
                                UserToken: UserToken,
                                Name: Name,
                                UserName: UserName,
                                Email: Email,
                                GroupId: GroupId,
                                Screens: Screens,
                                AdminFlag:AdminFlag,
                                GroupAdminFlag:GroupAdminFlag,
                                SuperAdminFlag:SuperAdminFlag

                            },
                            success: function (data) {
                                $(".overlay").hide();
                                if ($.trim(data) == 'Y') {
                                    window.location.href = 'edituser' + extension;
                                }
                            }
                        });
                    });
                    function DeleteUser() {

                        $(".overlay").show();
                        $.ajax({
                            type: "POST",
                            url: "ajax/DeleteUser" + extension,
                            data: {
                                UserToken: $('#hdnUserToken').val(),
                            },
                            success: function (data) {
                                $(".overlay").hide();
                                if ($.trim(data) == 'Y') {
                                    $('#success').removeClass('hide');
                                    $('.successmsg').text("SUCCESS : User Deleted Successfully");
                                    $('#success').fadeIn(1000).delay(3000).fadeOut(1000);
                                    var table = $('#tblUser').DataTable();
                                    table.destroy();
                                    table.clear();
                                    loadTable();
                                } else {
                                    $('#error').removeClass('hide');
                                    $('.errormsg').text("ERROR : " + data);
                                    $('#error').fadeIn(1000).delay(3000).fadeOut(1000);
                                }
                            }
                        });
                    }

                    function ChangePassword() {

                        var userPassword = $('#password').val();
                        var userCPassword = $('#cpassword').val();
                        if ($.trim(userPassword) == '') {
                            $('#password').attr("placeholder", "Please enter Password");
                            $('#password').addClass('error').focus();
                            return false;
                        } else if ($.trim(userCPassword) == '') {
                            $('#cpassword').attr("placeholder", "Please enter Confirm Password");
                            $('#cpassword').addClass('error').focus();
                            return false;
                        } else if ($.trim(userCPassword) != $.trim(userPassword)) {
                            $('#cpassword').attr("placeholder", "Not matching with Password");
                            $('#cpassword').addClass('error').focus();
                            return false;
                        } else {
                            $(".overlay").show();
                            $.ajax({
                                type: "POST",
                                url: "ajax/ResetPassword" + extension,
                                data: {
                                    UserToken: $('#hdnUserToken').val(),
                                    userPassword: userPassword,
                                },
                                success: function (data) {
                                    $(".overlay").hide();
                                    if ($.trim(data) == 'Y') {
                                        $('#success').removeClass('hide');
                                        $('.successmsg').text("SUCCESS : Password Changed Successfully");
                                        $('#success').fadeIn(1000).delay(3000).fadeOut(1000);
                                        var table = $('#tblUser').DataTable();
                                        table.destroy();
                                        table.clear();
                                        loadTable();
                                        $('#passwordModel').modal('close');
                                    } else {
                                        $('#error').removeClass('hide');
                                        $('.errormsg').text("ERROR : " + data);
                                        $('#error').fadeIn(1000).delay(3000).fadeOut(1000);
                                    }
                                }
                            });
                        }
                    }
        </script>

    </body>
</html>