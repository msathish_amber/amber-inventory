<?php
include("config/config.php");
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
        <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>User Login | Amber Inventory Portal</title>
        <link rel="apple-touch-icon" href="images/favicon/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- BEGIN: VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/login.css">
        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <!-- END: Custom CSS-->
    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 1-column login-bg  blank-page blank-page" data-open="click" data-menu="horizontal-menu" data-col="1-column">
        <div class="row">
            <div class="logoWrapper"><img src="images/logo/logo.png" alt=""></div>
            <div class="col s12">
                <div class="container"><div id="login-page" class="row">
                        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                            <form class="login-form">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <h5 class="ml-4">Sign in</h5>
                                    </div>
                                </div>
                                <div class="card-alert card gradient-45deg-amber-amber hide" id="error">
                                    <div class="card-content white-text">
                                        <p>
                                            <i class="material-icons">warning</i> <span class="errormsg"></span></p>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="row margin" id="divUser">
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix pt-2">person_outline</i>
                                        <input id="username" type="text">
                                        <label for="username" class="center-align">Username</label>
                                    </div>
                                </div>
                                <div class="row margin " id="divPass">
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix pt-2">lock_outline</i>
                                        <input id="password" type="password" onkeydown="if (event.keyCode == 13) {
                                                    Login()
                                                }">
                                        <label for="password">Enter OTP which sent to your mail</label>
                                    </div>
                                </div>

                                <div class="row" id="divbtn">
                                    <div class="input-field col s12 login-btn">
                                        <a href="javascript:;" id="btnLogin" onclick="Login();" class="btn waves-effect waves-light border-round col s12">Login</a>
                                    </div>
                                </div>    
                                <div class="row " id="divbtn2">
                                    <div class="input-field col s12 login-btn">
                                        <a href="javascript:;" id="btnLogin" onclick="LoginVerify();" class="btn waves-effect waves-light border-round col s12">Login</a>
                                    </div>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BEGIN VENDOR JS-->
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>
        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="js/scripts/ui-alerts.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
          <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <script type="text/javascript">
                                            var d = new Date()
                                            var offset = -d.getTimezoneOffset() * 60;
                                            var today = moment();
                                                    var curDate = today.format('YYYY-MM-DD');
                                                    var curDateTime = today.format('YYYY-MM-DD hh:mm:ss A');
                                               
                                            $('#password').focus();
                                            $('#divPass').hide();

                                            $('#divbtn2').hide();

                                            function Login() {

                                                var uName = $('#username').val();

                                                if (uName == '') {
                                                    $('#username').addClass('invalid_box').focus();
                                                    return false;
                                                } else {
                                                    $('.preloader-wrapper').addClass('active');

                                                    $('#btnLogin').eq(0).html('Loading...');
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "ajax/loginotp<?php echo $extension; ?>",
                                                        data: {
                                                            username: uName,
                                                        },
                                                        success: function (msg) {
                                                         
                                                            $('.preloader-wrapper').removeClass('active');
                                                            if ($.trim(msg) == 'N') {
                                                                $('#btnLogin').eq(0).text("LOGIN");
                                                                $('#error').removeClass('hide');
                                                                $('.errormsg').text("Error! The username you entered is incorrect");
                                                                $('#error').fadeIn(1000).delay(3000).fadeOut(1000);
                                                                $('#username').focus();
                                                            } else {
                                                                $('#btnLogin').eq(0).text("LOGIN");
                                                                $('#password').focus();
                                                                $('#divPass').show();
                                                                $('#divUser').hide();
                                                                $('#divbtn2').show();
                                                                $('#divbtn').hide();
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                            function LoginVerify() {

                                                var uName = $('#username').val();
                                                var uPass = $('#password').val();
                                                if (uName == '') {
                                                    $('#username').addClass('invalid_box').focus();
                                                    return false;
                                                } else if (uPass == '') {
                                                    $('#password').addClass('invalid_box').focus();
                                                    return false;
                                                } else {
                                                    $('.preloader-wrapper').addClass('active');

                                                    $('#btnLogin').eq(0).html('Loading...');
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "ajax/loginverify<?php echo $extension; ?>",
                                                        data: {
                                                            username: uName,
                                                            password: uPass,
                                                            offset: offset,
                                                            curDateTime:curDateTime
                                                        },
                                                        success: function (msg) {
                                                            $('.preloader-wrapper').removeClass('active');
                                                            if ($.trim(msg) == 'Y') {
                                                                window.location.href = "dashboard<?php echo $extension; ?>";
                                                            } else {
                                                                $('#btnLogin').eq(0).text("LOGIN");
                                                                $('#error').removeClass('hide');
                                                                $('.errormsg').text("Error! The OTP you entered is incorrect");
                                                                $('#error').fadeIn(1000).delay(3000).fadeOut(1000);
                                                                $('#username').focus();
                                                            }
                                                        }
                                                    });
                                                }
                                            }

        </script>
    </body>
</html>