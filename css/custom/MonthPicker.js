(function ($) {

    var monthPickers = $('input.monthpicker'),
            baseClass = 'monthpicker',
            yearClass = baseClass + '-year',
            monthClass = baseClass + '-month',
            previousClass = baseClass + '-previous',
            nextClass = baseClass + '-next',
            yearValueClass = baseClass + '-year-value',
            monthValueClass = baseClass + '-month-value',
            openClass = baseClass + '--open',
            closingClass = baseClass + '--closing';

    var template = $('<div class="' + baseClass + '">\
                      <div class="' + yearClass + '">\
                        <button type="button" class="' + previousClass + '"></button>\
                        <div class="' + yearValueClass + '"></div>\
                        <button type="button" class="' + nextClass + '"></button>\
                      </div>\
                      <div class="' + monthClass + '">\
                        <button type="button" class="' + previousClass + '"></button>\
                        <div class="' + monthValueClass + '"></div>\
                        <button type="button" class="' + nextClass + '"></button>\
                      </div>\
                    </div>');

    var monthPicker = (function ($) {

        var clone,
                input,
                currentDate = new Date(),
                month = currentDate.getMonth() + 1,
                year = currentDate.getFullYear();

        var init = function (element) {

            input = element;
            var hasMonthpicker = $(input).data(baseClass);
            if (hasMonthpicker && hasMonthpicker != '') {

                return;
            }

            $(input).data(baseClass, new Date().getTime());
            clone = template.clone();
            clone.insertAfter(input);

            clone.css('left', input.offsetLeft);
            clone.css('top', input.offsetTop + input.offsetHeight);

            initEvents();
            update();
            clone.addClass(openClass);
        };

        var update = function () {

            year = year >= 0 ? year : 0;

            if (month > 12) {

                month = 1;
                year++;
            } else if (month < 1) {

                month = 12;
                year--;
            }

            $('.' + yearValueClass, clone).html(year);
            $('.' + monthValueClass, clone).html(month);
        };

        var close = function () {

            $('body').off('click', onBlur);

            if (clone && !clone.hasClass(closingClass)) {

                clone.removeClass(openClass);
                clone.addClass(closingClass);
                setTimeout(function () {

                    $(input).data(baseClass, '');
                    clone.remove();
                    clone = null;
                }, 300);
            }
        };

        var onBlur = function (event) {

            var target = $(event.target);

            if (!target.hasClass(baseClass) && target.parents('.' + baseClass).length < 1) {

                close();
            }
        };

        var updateInput = function () {

            $(input).val(year + "-" + ("0" + month).substr(-2));
            $(input).trigger('change');
            $(input).trigger('input');
        };

        var initEvents = function () {

            var changeValue = function (event) {

                if (event.data.type == 'year')
                    year += event.data.value;
                else
                    month += event.data.value;

                update();
                updateInput();
            };

            $('.' + yearClass + ' .' + previousClass, clone).on('click', {value: 1, type: 'year'}, changeValue);
            $('.' + yearClass + ' .' + nextClass, clone).on('click', {value: -1, type: 'year'}, changeValue);
            $('.' + monthClass + ' .' + previousClass, clone).on('click', {value: 1, type: 'month'}, changeValue);
            $('.' + monthClass + ' .' + nextClass, clone).on('click', {value: -1, type: 'month'}, changeValue);

            $('body').on('click', onBlur);
        };

        return init;
    })($);

    monthPickers.on('focus', function (event) {

        monthPicker(this);
    });

})(jQuery);