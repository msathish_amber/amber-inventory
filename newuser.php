<?php
include("config/config.php");
if (empty($_SESSION['Amber_Inventory_UserToken'])) {
    header("Location: index" . $extension);
}
if ($_SESSION['Amber_Inventory_Screen_UserCreate'] != 'Y') {
    header("Location: index" . $extension);
}
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
        <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>Amber-Inventory</title>
        <link rel="amber-icon" href="images/favicon/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- BEGIN: VENDOR CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/flag-icon/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">

        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/data-tables.css">
        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
        <!--END: Custom CSS-->
        <style>
            .select-wrapper>ul{
                max-height: 200px !important;
            }
            #divFilter span.select2.select2-container.select2-container--default{
                padding-top:10px;
            }
            #divFilter span.select2-selection__arrow{
                top:10px;
            }
            [type='checkbox'] + span:not(.lever){
                padding-left:25px;
            }
        </style>
    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        <!-- BEGIN: Header-->
        <?php include("header.php"); ?>
        <!-- END: Header-->

        <!-- BEGIN: Page Main-->
        <div id="main">
            <div class="row">
                <div class="col s12">
                    <div class="container">

                        <div class="row">
                            <div class="overlay">
                            </div>
                            <div class="col s12 m6 l6">
                                <h5 class="breadcrumbs-title">Create New User</h5>

                            </div>

                            <div class="col s12 m12 l12">
                                <div id="button-trigger" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <div class="card-alert card gradient-45deg-amber-amber hide" id="error">
                                            <div class="card-content white-text">
                                                <p>
                                                    <i class="material-icons">warning</i> <span class="errorMsg"></span></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="card-alert card gradient-45deg-amber-amber hide" id="success">
                                            <div class="card-content white-text">
                                                <p>
                                                    <i class="material-icons">done</i> <span class="successMsg">User Added Successfully</span></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="row">

                                            <div class="col s12">
                                                <div class="input-field col s4">
                                                    <input id="name" type="text">
                                                    <label for="name">Name</label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="username" type="text">
                                                    <label for="username">User Name</label>
                                                </div>

                                                <div class="input-field col s4">
                                                    <select id="ddlGroup">
                                                        <option value="" disabled selected>Choose Group</option>
                                                        <?php
                                                        $usersGroupUrl = $service_domain . "inventory/listgroups";

                                                        $usersGroupData = getData($usersGroupUrl, $commonPostArray);
                                                        $usersGroupJson = json_decode($usersGroupData);
                                                        $usersGroupArray = $usersGroupJson->Items;
                                                        $usersGroupCount = count($usersGroupArray);
                                                        ?>

                                                        <?php
                                                        for ($ci = 0; $ci < $usersGroupCount; $ci++) {
                                                            ?>
                                                            <option value="<?php echo $usersGroupArray[$ci]->AdminGroupId; ?>"><?php echo $usersGroupArray[$ci]->GroupName; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <label>Group</label>
                                                </div>

                                                <div class="input-field col s4">
                                                    <input id="email" type="text">
                                                    <label for="email">Email-Id</label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="password" type="password">
                                                    <label for="password">Password</label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="cpassword" type="password">
                                                    <label for="cpassword">Confirm Password</label>
                                                </div>
                                                <div class="input-field col s12">
                                                    <p>Portal Screen Access</p><br>
                                                    <p>
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="DASHBOARD" id="chkDashboard"/>
                                                            <span>Dashboard</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="STOCKREPORT" id="chkStockReport"/>
                                                            <span>Global Stock Report</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="SUBSCRIPTIONREPORT" id="chkSubscriptionReport"/>
                                                            <span>Subscription Forecasting</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="DEVICEACTIVATION" id="chkDeviceActivation"/>
                                                            <span>Device Activation</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="SHIELDACTIVATION" id="chkShieldActivation"/>
                                                            <span>Shield Activation</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="DEVICERENEWAL" id="chkDeviceRenewal"/>
                                                            <span>Device Renewal</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="SHIELDRENEWAL" id="chkShieldRenewal"/>
                                                            <span>Shield Renewal</span>
                                                        </label>
                                                    </p><br>
                                                    <p>
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="MONTHLYBILLING" id="chkMonthlyBillable"/>
                                                            <span>Mothly Billable Subscriptions</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="UNITSNOTWORKING" id="chkUnitsNotWorking"/>
                                                            <span>Units Not Working</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="OPENAPI" id="chkOpenApi"/>
                                                            <span>Open API Usage Report</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkboxuser" value="VIEW" id="chkStockReport"/>
                                                            <span>Users</span>
                                                        </label>
                                                        <label style="font-weight: bold;font-size: 20px;color:black;">&nbsp;(&nbsp;<label>
                                                                <input type="checkbox" name="chkboxuser" value="CREATE" id="chkCreateUser"/>
                                                                <span>Create</span>
                                                            </label> &nbsp; &nbsp;
                                                            <label>
                                                                <input type="checkbox" name="chkboxuser" value="EDIT" id="chkEditUser"/>
                                                                <span>Edit</span>
                                                            </label> &nbsp; &nbsp;
                                                            <label>
                                                                <input type="checkbox" name="chkboxuser" value="DELETE" id="chkDeleteUser"/>
                                                                <span>Delete</span>
                                                            </label> &nbsp; &nbsp;
                                                            <label>
                                                                <input type="checkbox" name="chkboxuser" value="RESET_PASSWORD" id="chkResetPassword"/>
                                                                <span>Reset Password</span>
                                                            </label>
                                                            )</label>
                                                    </p><br>
                                                     <p>
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="HELPMEACTIVATION" id="chkHA"/>
                                                            <span>HelpMe! Activation Report</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="HELPMERENEWAL" id="chkHR"/>
                                                            <span>HelpMe! Renewal Report</span>
                                                        </label>
                                                        &nbsp; &nbsp;
                                                        <label>
                                                            <input type="checkbox" name="chkbox" value="HELPMEUSERS" id="chkHU"/>
                                                            <span>HelpMe! Users</span>
                                                        </label>
                                                     </p>
                                                </div>
                                                <?php if ($_SESSION['Amber_Inventory_SuperAdminFlag'] == 'Y') { ?>
                                                    <div class="input-field col s12">
                                                        <p>Admin Flag Access</p><br>
                                                        <p>
                                                            <label>
                                                                <input type="checkbox" name="chkboxAdmin" value="ADMINFLAG" id="chkAdminFlag"/>
                                                                <span>Admin Flag</span>
                                                            </label>
                                                            &nbsp; &nbsp;
                                                            <label>
                                                                <input type="checkbox" name="chkboxAdmin" value="GROUPADMINFLAG" id="chkGroupAdminFlag"/>
                                                                <span>Group Admin Flag</span>
                                                            </label>
                                                            &nbsp; &nbsp;
                                                            <label >
                                                                <input type="checkbox" name="chkboxAdmin" value="SUPERADMINFLAG" id="chkSuperAdminFlag"/>
                                                                <span>Super Admin Flag</span>
                                                            </label>
                                                        </p>
                                                    </div>
                                                <?php } ?>
                                                <div class="input-field col s12">
                                                    <a class="btn waves-effect waves-light right" onclick="CreateUser()">Create User
                                                        <i class="material-icons right">send</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->

        <!-- BEGIN: Footer-->
        <?php include("footer.php"); ?>
        <!-- END: Footer-->

        <!-- END: Footer-->
        <!-- BEGIN VENDOR JS-->
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script src="vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>
        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="js/scripts/data-tables.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" type="text/javascript"></script>
        <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jszip.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/FileSaver.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/myexcel.js?version=<?php echo $jsVersion; ?>"></script>
        <script>
                                                        $(function () {
                                                            $("select").formSelect();
                                                        });

                                                        function CreateUser() {
                                                            var name = $('#name').val();
                                                            var userName = $('#username').val();
                                                            var groupUser = $('#ddlGroup option:selected').val();
                                                            var userEmail = $('#email').val();
                                                            var userPassword = $('#password').val();
                                                            var userCPassword = $('#cpassword').val();
                                                            var screenname = [];
                                                            var screenvalue = [];
                                                            var screenusername = [];
                                                            var screenuservalue = [];
                                                            var screenadminname = [];
                                                            var screenadminvalue = [];
                                                            $('input:checkbox[name=chkbox]').each(function ()
                                                            {
                                                                if ($(this).is(':checked')) {

                                                                    screenname.push($(this).val());
                                                                    screenvalue.push('Y');
                                                                } else {
                                                                    screenname.push($(this).val());
                                                                    screenvalue.push('N');
                                                                }

                                                            });

                                                            $('input:checkbox[name=chkboxuser]').each(function ()
                                                            {
                                                                if ($(this).is(':checked')) {

                                                                    screenusername.push($(this).val());
                                                                    screenuservalue.push('Y');
                                                                } else {
                                                                    screenusername.push($(this).val());
                                                                    screenuservalue.push('N');
                                                                }

                                                            });
                                                            var AdminFlag = 'N';
                                                            if ($('#chkAdminFlag').is(':checked')) {
                                                                AdminFlag = 'Y';
                                                            }
                                                            var SuperAdminFlag = 'N';
                                                            if ($('#chkSuperAdminFlag').is(':checked')) {
                                                                SuperAdminFlag = 'Y';
                                                            }
                                                            var GroupAdminFlag = 'N';

                                                            if ($('#chkGroupAdminFlag').is(':checked')) {
                                                                GroupAdminFlag = 'Y';
                                                            }

                                                            regex1 = /^[^\s\n@]*[^\s\n\.@]\@[^\s\n\.@][^\s\n@]*(?=\.[^\s\.\n @]+$)\.[^\s\.\n @]+$/;
                                                            regex2 = /(?:\.\.)/;

                                                            if ($.trim(name) == '') {
                                                                $('#name').attr("placeholder", "Please enter Name");
                                                                $('#name').addClass('error').focus();
                                                                return false;
                                                            } else if ($.trim(userName) == '') {
                                                                $('#username').attr("placeholder", "Please enter User Name");
                                                                $('#username').addClass('error').focus();
                                                                return false;
                                                            } else if ($.trim(groupUser) == '') {

                                                                $('.dropdown-trigger').addClass('error').focus();
                                                                return false;
                                                            } else if ($.trim(userEmail) == '') {
                                                                $('#email').attr("placeholder", "Please enter Email");
                                                                $('#email').addClass('error').focus();
                                                                return false;
                                                            } else if (!userEmail.match(regex1) || userEmail.match(regex2)) {
                                                                $('#email').attr("placeholder", "Please enter a Valid Email");
                                                                $('#email').addClass('error').focus();
                                                                return false;
                                                            } else if ($.trim(userPassword) == '') {
                                                                $('#password').attr("placeholder", "Please enter Password");
                                                                $('#password').addClass('error').focus();
                                                                return false;
                                                            } else if ($.trim(userCPassword) == '') {
                                                                $('#cpassword').attr("placeholder", "Please enter Confirm Password");
                                                                $('#cpassword').addClass('error').focus();
                                                                return false;
                                                            } else if ($.trim(userCPassword) != $.trim(userPassword)) {
                                                                $('#cpassword').attr("placeholder", "Not matching with Password");
                                                                $('#cpassword').addClass('error').focus();
                                                                return false;
                                                            } else {
                                                                $(".overlay").show();
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "ajax/createUser" + extension,
                                                                    data: {
                                                                        name: name,
                                                                        userName: userName,
                                                                        groupUser: groupUser,
                                                                        userEmail: userEmail,
                                                                        userPassword: userPassword,
                                                                        screenname: screenname,
                                                                        screenvalue: screenvalue,
                                                                        screenusername: screenusername,
                                                                        screenuservalue: screenuservalue,
                                                                        AdminFlag: AdminFlag,
                                                                        SuperAdminFlag: SuperAdminFlag,
                                                                        GroupAdminFlag: GroupAdminFlag
                                                                    },
                                                                    success: function (data) {
                                                                        $(".overlay").hide();
                                                                        var results = data.split("~~");
                                                                        if ($.trim(results[0]) == 'Y') {
                                                                            $('#success').removeClass('hide');
                                                                            $('.successMsg').fadeIn(1000).delay(3000).fadeOut(1000);

                                                                            setTimeout(function () {
                                                                                window.location.href = 'users' + extension;
                                                                            }, 3000);
                                                                        } else {
                                                                            $('#error').removeClass('hide');
                                                                            $('.errorMsg').text("Error! " + results[0]);
                                                                            $('.error').fadeIn(1000).delay(3000).fadeOut(1000);
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
        </script>

    </body>
</html>