<?php
include("config/config.php");

if(empty($_SESSION['Amber_Inventory_UserToken']) && $_SESSION['Amber_Inventory_Screen_Dashboard'] != 'Y') {
	header("Location: index".$extension);
}
$PostUrl = $service_domain . "inventory/getdashboard";
$ResultAray = array();
$ResultData = getData($PostUrl, $commonPostArray);
$ResultJson = json_decode($ResultData);

$TotalDeviceCount = $ResultJson->TotalDeviceCount;
$TotalSoldCount = $ResultJson->TotalSoldCount;
$TotalSubscription = $ResultJson->TotalSubscription;
$ActiveSubscription = $ResultJson->ActiveSubscription;
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
        <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>Amber :: Inventory</title>
        <link rel="amber-icon" href="images/favicon/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- BEGIN: VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/animate-css/animate.css">
        <link rel="stylesheet" type="text/css" href="vendors/chartist-js/chartist.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/chartist-js/chartist-plugin-tooltip.css">
        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/dashboard.css">

        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        <!-- BEGIN: Header-->
        <?php include("header.php"); ?>
        <!-- END: Header-->

        <!-- BEGIN: Page Main-->
        <div id="main">
            <div class="row">
                <div class="col s12">
                    <div class="container">
                        <!--card stats start-->
                        <div id="card-stats">
                            <div class="row">
                                <div class="col s12 m6 l6 xl3">
                                    <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text animate fadeLeft">
                                        <div class="padding-4">
                                            <div class="col s4 m4">
                                                <i class="material-icons background-round mt-5">timeline</i>

                                            </div>
                                            <div class="col  s8 m8 right-align">
                                                <h5 class="mb-0 white-text"><?php echo number_format($TotalDeviceCount); ?></h5>
                                                <p class="no-margin">Total Devices</p>
                                                <p>&nbsp;</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l6 xl3">
                                    <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text animate fadeLeft">
                                        <div class="padding-4">
                                            <div class="col s4 m4">
                                                <i class="material-icons background-round mt-5">timeline</i>

                                            </div>
                                            <div class="col  s8 m8 right-align">
                                                <h5 class="mb-0 white-text"><?php echo number_format($TotalSoldCount); ?></h5>
                                                <p class="no-margin">Total Sold Devices</p>
                                                <p>&nbsp;</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l6 xl3">
                                    <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text animate fadeRight">
                                        <div class="padding-4">
                                            <div class="col s4 m4">
                                                <i class="material-icons background-round mt-5">timeline</i>

                                            </div>
                                            <div class="col  s8 m8 right-align">
                                                <h5 class="mb-0 white-text"><?php echo number_format($TotalSubscription); ?></h5>
                                                <p class="no-margin">Total Subscriptions</p>
                                                <p>&nbsp;</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l6 xl3">
                                    <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text animate fadeRight">
                                        <div class="padding-4">
                                            <div class="col s4 m4">
                                                <i class="material-icons background-round mt-5">timeline</i>

                                            </div>
                                            <div class="col s8 m8 right-align">
                                                <h5 class="mb-0 white-text"><?php echo number_format($ActiveSubscription); ?></h5>
                                                <p class="no-margin">Active Subscriptions</p>
                                                <p>&nbsp;</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--card stats end-->
                        <!--yearly & weekly revenue chart start-->
                        <div id="sales-chart">
                            <div class="row">
                                <div class="col s12 m12 22">
                                    <div id="revenue-chart" class="card animate fadeUp">
                                        <div class="card-content">
                                            <h4 class="header mt-0">
                                                Total Device Activation - <span style="font-size: small;padding: 2px;vertical-align:middle;" id='spnDateActivation'></span>

                                            </h4>


                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="sample-chart-wrapper"><canvas id="line-chart" height="400"></canvas></div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col s12 m12 22">
                                    <div id="revenue-chart" class="card animate fadeUp">
                                        <div class="card-content">
                                            <h4 class="header mt-0">
                                                Total Stock Report - <span id='spnDateTotal' style="font-size: small;vertical-align:middle; padding: 2px;"></span>

                                            </h4>


                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="sample-chart-wrapper">     <canvas id="canvas-TotalStock"></canvas></div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->
        <input type="hidden" id="hdnCustomStart" name="hdnCustomStart" value="">
        <input type="hidden" id="hdnCustomEnd" name="hdnCustomEnd" value="">
        <input type="hidden" id="hdnGroupId" name="hdnGroupId" value="">
        <input type="hidden" id="hdnCountry" name="hdnCountry" value="">
        <input type="hidden" id="hdnFilterType" name="hdnFilterType" value="Day">
        <!-- BEGIN: Footer-->
        <?php include("footer.php"); ?>
        <!-- END: Footer-->

        <!-- BEGIN VENDOR JS-->
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="vendors/chartjs/chart.min.js"></script>
        <script src="vendors/chartist-js/chartist.min.js" type="text/javascript"></script>
        <script src="vendors/chartist-js/chartist-plugin-tooltip.js" type="text/javascript"></script>
        <script src="vendors/chartist-js/chartist-plugin-fill-donut.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>
        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script>


            var lastdatenew = moment().subtract(29, 'days').format('YYYY-MM-DD') + " 12:00:00 am";
            var currentdate = moment().format('YYYY-MM-DD') + " 11:59:59 pm";
            $('#hdnCustomStart').val(lastdatenew);
            $('#hdnCustomEnd').val(currentdate);
            $('#spnDateActivation').text(" (" + lastdatenew + " - " + currentdate + ")");
            $('#spnDateTotal').text(" (" + lastdatenew + " - " + currentdate + ")");
            
            function getActivationGraph() {

                $.ajax({
                    type: "POST",
                    url: "ajax/ActivationReportAll" + extension,
                    data: {
                        offset: offset,
                        Country: $('#hdnCountry').val(),
                        FilterGroupId: $('#hdnGroupId').val(),
                        FilterType: $('#hdnFilterType').val(),
                        CustomStartDate: $('#hdnCustomStart').val(),
                        CustomEndDate: $('#hdnCustomEnd').val(),
                    },
                    success: function (data) {

                        var results = data.split("~~");
                        if ($.trim(results[0]) == 'Y') {

                            var parsedData = $.parseJSON(results[2]);
                            var parsedCount = $.parseJSON(results[3]);


                            var ctx = $("#line-chart");
                            // Chart Options
                            var chartOptions = {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    position: "bottom"
                                },
                                hover: {
                                    mode: "label"
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            display: true,
                                            gridLines: {
                                                color: "#f3f3f3",
                                                drawTicks: false,

                                            },
                                            scaleLabel: {
                                                display: true,
                                                labelString: "Day",

                                            },
                                            ticks: {
                                                padding: 15
                                            }
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            display: true,
                                            gridLines: {
                                                color: "#f3f3f3",
                                                drawTicks: false
                                            },
                                            scaleLabel: {
                                                display: true,
                                                labelString: "Value",

                                            }
                                        }
                                    ]
                                },
                                title: {
                                    display: true,

                                }
                            };
                            // Chart Data
                            var chartData = {
                                labels: parsedData,
                                datasets: [
                                    {
                                        label: "Device Activation",
                                        data: parsedCount,
                                        fill: false,
                                        borderColor: "#e91e63",
                                        pointBorderColor: "#e91e63",
                                        pointBackgroundColor: "#FFF",
                                        pointBorderWidth: 2,
                                        pointHoverBorderWidth: 2,
                                        pointRadius: 4
                                    },
                                ],

                            };
                            var config = {
                                type: "line",
                                // Chart Options
                                options: chartOptions,
                                data: chartData
                            };
                            // Create the chart
                            var lineChart = new Chart(ctx, config);
                        }
                    }
                });

            }
            getActivationGraph();
        </script>

        <script>
            var B2BDataCount, BlankDataCount, B2CDataCount;
            function getTotalStock() {

                $.ajax({
                    type: "POST",
                    url: "ajax/Dashboard" + extension,
                    data: {
                        offset: offset,
                        Country: $('#hdnCountry').val(),
                        FilterGroupId: $('#hdnGroupId').val(),
                        FilterType: $('#hdnFilterType').val(),
                        CustomStartDate: $('#hdnCustomStart').val(),
                        CustomEndDate: $('#hdnCustomEnd').val(),
                    },
                    success: function (data) {

                        var results = data.split("~~");
                        if ($.trim(results[0]) == 'Y') {

                            var GroupData = $.parseJSON(results[1]);
                            var TotalDataCount = $.parseJSON(results[2]);
                            var SoldDataCount = $.parseJSON(results[3]);
                            var InstockDataCount = $.parseJSON(results[4]);
                            B2BDataCount = $.parseJSON(results[5]);
                            B2CDataCount = $.parseJSON(results[6]);
                            BlankDataCount = $.parseJSON(results[7]);
                            var barChartData = {
                                labels: GroupData,
                                datasets: [
                                    {
                                        label: "Total Stock",
                                        backgroundColor: "pink",
                                        borderColor: "red",
                                        borderWidth: 1,
                                        data: TotalDataCount,
                                        minHeight: 20,
                                    },
                                    {
                                        label: "Sold Devices",
                                        backgroundColor: "lightblue",
                                        borderColor: "blue",
                                        borderWidth: 1,
                                        data: SoldDataCount,
                                        minHeight: 20,
                                    },
                                    {
                                        label: "In Stock Devices",
                                        backgroundColor: "lightgreen",
                                        borderColor: "green",
                                        borderWidth: 1,
                                        data: InstockDataCount,
                                        minHeight: 20,
                                    },
                                ]
                            };

                            var chartOptions = {
                                responsive: true,
                                legend: {
                                    position: "top"
                                },

                                tooltips: {
                                    mode: 'index',

                                    callbacks: {
                                        index: function (tooltipItem, data) {
                                            let label = data.datasets[tooltipItem.datasetIndex].label;
                                            let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                            return label + ': ' + value;

                                        },
                                        footer: function (tooltipItems, data) {

                                            return ['B2B : ' + B2BDataCount[tooltipItems[0].index], 'B2C : ' + B2CDataCount[tooltipItems[0].index], 'Blank : ' + BlankDataCount[tooltipItems[0].index]];
                                        }
                                    },
                                    intersect: false
                                },
                                title: {
                                    display: true,
                                    text: "Total Stock Report"
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            display: true,
                                            gridLines: {
                                                display: false
                                            },
                                            ticks: {
                                                autoSkip: false,
                                                padding: 10
                                            }
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            display: true,

                                            gridLines: {
                                                display: false
                                            },
                                            ticks: {
                                                beginAtZero: true,

                                            }
                                        }
                                    ]
                                },

                            }

                            var ctx = document.getElementById("canvas-TotalStock").getContext("2d");
                            window.myBar = new Chart(ctx, {
                                type: "bar",
                                data: barChartData,
                                options: chartOptions
                            });


                        }
                    }
                });

            }


            getTotalStock();
        </script>
    </body>
</html>