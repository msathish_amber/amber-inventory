<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
    <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
</head>
<body>
    <div class="undermaintenance-bg">
						<img src="images/maintenance-info.jpg" alt="">
						<div class="undermaintence-text">
							This <strong>Thursday, December 12th,</strong> Amber Connect will perform a <strong>planned annual server maintenance.</strong> The maintenance window will last from <strong>11:00 to 21:00 UTC.</strong> Your account will be affected by this maintenance.<br><br>
							<strong>Expected Behavior:</strong><br><br>
							Amber Connect customers will experience a service disruption, followed by degraded performance within the 9-hours maintenance window. It may also cause complete downtime up to 30 mins at the start and end of the maintenance process. Amber Connect Apps, Portals and Amber Shield will not be fully operational during the scheduled maintenance window. However, You can access the live location of the vehicles except 2 instances of 30 mins complete downtime. All the trips will be recorded in the background and restored after the maintenance window. <br><br>
                            <strong>Why We're Doing This:</strong>
                            <ul>
                                <li>Improve stability</li>
                                <li>Enhance security</li>
                                <li>Accelerate data processing speed</li>
                                <li>Optimise performance</li>
                                <li>Enable new technologies</li>
                            </ul>                            
                            Amber Connect Network engineering is augmenting our server infrastructure to support the ongoing demand of our service. Our intent is to increase connections per second (CPS) for ingress traffic and redirecting connections through a more robust and efficient firewall solution. You may experience service disruption and degraded performance during this period. We will strive to limit all service impacting events to less than 7 hours. 
                            <br></br>
							Please contact our help desk via in-app chat or email <a href="mailto:support@amberconnect.com" style="color: #EF6000;">support@amberconnect.com</a> or call <strong>+1-(876)-620-5255</strong> for any questions or concerns you may have regarding this information.<br><br><br>
						</div>
					</div>
</body>
</html>