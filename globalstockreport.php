<?php
include("config/config.php");
if (empty($_SESSION['Amber_Inventory_UserToken']) && $_SESSION['Amber_Inventory_Screen_StockReport'] != 'Y') {
    header("Location: index" . $extension);
}
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
        <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title>Amber-Inventory</title>
        <link rel="amber-icon" href="images/favicon/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- BEGIN: VENDOR CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/flag-icon/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">

        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/data-tables.css">
        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
        <!--END: Custom CSS-->
        <style>
            .select-wrapper>ul{
                max-height: 200px !important;
            }
            #divFilter span.select2.select2-container.select2-container--default{
                padding-top:10px;
            }
            #divFilter span.select2-selection__arrow{
                top:10px;
            }
        </style>
    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        <!-- BEGIN: Header-->
        <?php include("header.php"); ?>
        <!-- END: Header-->

        <!-- BEGIN: Page Main-->
        <div id="main">
            <div class="row">
                <div class="col s12">
                    <div class="container">

                        <div class="row">
                            <div class="overlay">
                            </div>
                            <div class="div col s5 offset-s3" id="progress" style=" position: fixed; top: 50%;z-index: 99999; background: white;">
                                <span>    Downloading Progress</span><span id="spnProgress" style="float:right;">0%</span>
                                <div class="progress">

                                    <div class="determinate" id="divProgress" ></div>
                                </div>
                            </div>
                            <div class="col s12 m12 l12">
                                <div id="button-trigger" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title">Global Stock Report</h4>
                                        <div class="">
                                            <div class="">
                                                <div class="col s3" id="divFilter">
                                                    <label>Filter By</label>
                                                    <select id="ddlFilter" onchange="ChangeFilter()">
                                                        <option value="Group">Group</option>
                                                        <option value="Country">Country</option>
                                                    </select>
                                                </div>

                                                <div class="col s3" id="divGroup">
                                                    <label>Filter By Group</label>
                                                    <select multiple id="ddlGroup" >

                                                        <?php
                                                        $usersGroupUrl = $service_domain . "inventory/listgroups";

                                                        $usersGroupData = getData($usersGroupUrl, $commonPostArray);
                                                        $usersGroupJson = json_decode($usersGroupData);
                                                        $usersGroupArray = $usersGroupJson->Items;
                                                        $usersGroupCount = count($usersGroupArray);
                                                        ?>

                                                        <?php
                                                        for ($ci = 0; $ci < $usersGroupCount; $ci++) {
                                                            ?>
                                                            <option value="<?php echo $usersGroupArray[$ci]->GroupId; ?>"><?php echo $usersGroupArray[$ci]->GroupName; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                                <div class="col s3" id="divCountry">
                                                    <label>Filter By Country</label>
                                                    <select multiple id="ddlCountry" >

                                                        <?php
                                                        $countryUrl = $service_domain . "inventory/getcountrylist";

                                                        $countryData = getData($countryUrl, $commonPostArray);
                                                        $countryJson = json_decode($countryData);
                                                        $countryArray = $countryJson->Items;
                                                        $countryCount = count($countryArray);
                                                        ?>

                                                        <?php
                                                        for ($ci = 0; $ci < $countryCount; $ci++) {
                                                            ?>
                                                            <option value="<?php echo $countryArray[$ci]->Country; ?>"><?php echo $countryArray[$ci]->Country; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                                <div class="input-field col s5"> <a class="mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan" style="background: -webkit-linear-gradient(45deg, #0288d1, #26c6da) !important;" onclick="ApplyFilters();">Apply</a>
                                                    <a class="mb-6 btn waves-effect waves-light gradient-45deg-red-pink" onclick="ClearFilters();">Clear</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col s12">
                                                <div class="table-responsive" style="overflow: auto;">
                                                    <table id="tblStockReport"  class="display nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th>AmberAuthToken</th>
                                                                <th>IMEI</th>
                                                                <th>Sim Number</th>
                                                                <th>Admin User</th>
                                                                <th>Country</th>
                                                                <th>Active Status</th>
                                                                <th>Retail Phone</th>

                                                                <th>Amber Model</th>
                                                                <th>Sim Provider</th>
                                                                <th>Device Type</th>
                                                                <th>User EmailId</th>
                                                                <th>User Phone</th>
                                                                <th>Phone Number</th>
                                                                <th>Retail Name</th>
                                                                <th>App UserName</th>
                                                                <th>User Mapped Date</th>
                                                                <th>Last Heart Beat</th>
                                                                <th>Retail Date</th>
                                                                <th>Updated Date</th>
                                                                <th>Fault Device</th>
                                                                <th>Group Name</th>                                                              
                                                                <th>Fleet Id</th>
                                                                <th>Fleet Name</th>
                                                                <th>Retail Kyc</th>
                                                                <th>In-Transit</th>
                                                                <th>Subscription Start Date</th>
                                                                <th>Subscription End Date</th>
                                                                <th>Data Packet</th>
                                                                <th>Vehicle Name</th>
                                                                <th>Plate No</th>
                                                                <th>Retail User Name</th>
                                                                <th>Sold By</th>
                                                                <th>Retail GroupName</th>
                                                                <th>Retail Country</th>
                                                                <th>Purchase Date</th>

                                                                <th>Remarks</th>
                                                                <th>Shield Sub Start Date</th>
                                                                <th>Shield Sub End Date</th>

                                                                <th>Subscription Type</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="tblbodyStock">

                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->
        <input type="hidden" id="hdnGroupId" name="hdnGroupId" value="">
        <input type="hidden" id="hdnCountry" name="hdnCountry" value="">
        <input type="hidden" id="hdnDateRange" name="hdnDateRange" value="">
        <input type="hidden" id="hdnTotalRecords" name="hdnTotalRecords" value="">
        <input type="hidden" id="hdnStartCount" name="hdnStartCount" value="0">
        <input type="hidden" id="hdnExportClicked" name="hdnExportClicked" value="N">
        <input type="hidden" id="hdnLimitExport" name="hdnLimitExport" value="500">
        <!-- BEGIN: Footer-->
        <?php include("footer.php"); ?>
        <!-- END: Footer-->

        <!-- END: Footer-->
        <!-- BEGIN VENDOR JS-->
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script src="vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>
        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="js/scripts/data-tables.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" type="text/javascript"></script>
        <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jszip.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/FileSaver.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/myexcel.js?version=<?php echo $jsVersion; ?>"></script>

        <script>
                                                        function ChangeFilter() {
                                                            $('#hdnCountry').val('');
                                                            $('#hdnGroupId').val('');
                                                            $("#ddlGroup").val(null).trigger('change');
                                                            $("#ddlCountry").val(null).trigger('change');
                                                            var radioValue = $("#ddlFilter").val();
                                                            if (radioValue == 'Group') {
                                                                $('#divGroup').show();
                                                                $('#divCountry').hide();

                                                            } else {
                                                                $('#divGroup').hide();
                                                                $('#divCountry').show();

                                                            }

                                                        }
                                                        $('#divCountry').hide();
                                                        $(document).ready(function () {

                                                            $('select').select2({width: "100%"});

                                                            loadTable();
                                                        });
                                                        function loadTable() {
                                                            $('#tblStockReport').DataTable({
                                                                dom: 'Bfrtip',
                                                                buttons: [
                                                                    "colvis",
                                                                    {
                                                                        extend: 'excelHtml5',
                                                                        action: generateajaxcall,
                                                                        text: 'EXPORT IN XLS',
                                                                    },
                                                                ],
                                                                "processing": true,
                                                                "serverSide": true,
                                                                "bLengthChange": false,
                                                                "responsive": true,
                                                                "searching": false,
                                                                "ordering": false,
                                                                ajax: {
                                                                    url: "ajax/StockReport" + extension,
                                                                    cache: false,
                                                                    type: "POST",
                                                                    data: {
                                                                        offset: offset,
                                                                        Country: $('#hdnCountry').val(),
                                                                        FilterGroupId: $('#hdnGroupId').val(),

                                                                    },
                                                                },
                                                                "initComplete": function (settings, json) {

                                                                    var info = this.api().page.info();
                                                                    $('#hdnTotalRecords').val(info.recordsTotal);

                                                                },
                                                                columns: [
                                                                    {data: "AmberAuthToken"},
                                                                    {data: "IMEI"},
                                                                    {data: "SimNumber"},
                                                                    {data: "AdminUserName"},
                                                                    {data: "Country"},
                                                                    {data: "ActiveStatus"},
                                                                    {data: "RetailPhone"},
                                                                    {data: "AmberModel"},
                                                                    {data: "SimProvider"},
                                                                    {data: "DeviceType"},
                                                                    {data: "UserEmailId"},
                                                                    {data: "UserPhone"},
                                                                    {data: "PhoneNumber"},
                                                                    {data: "RetailName"},
                                                                    {data: "AppUserName"},
                                                                    {data: "UserMappedDate"},
                                                                    {data: "HBTDate"},
                                                                    {data: "RetailDate"},
                                                                    {data: "UpdatedDate"},
                                                                    {data: "FaultDevice"},
                                                                    {data: "AdminGroupName"},
                                                                    {data: "FleetId"},
                                                                    {data: "FleetName"},
                                                                    {data: "RetailKyc"},
                                                                    {data: "InTransit"},
                                                                    {data: "SubscriptionStartDate"},
                                                                    {data: "SubscriptionEndDate"},
                                                                    {data: "DataPacket"},
                                                                    {data: "VehicleName"},
                                                                    {data: "PlateNo"},
                                                                    {data: "RetailUser"},
                                                                    {data: "SoldBy"},
                                                                    {data: "RetailGroupName"},
                                                                    {data: "RetailCountry"},
                                                                    {data: "PurchaseDate"},

                                                                    {data: "Remarks"},
                                                                    {data: "ShieldSubStartDate"},
                                                                    {data: "ShieldSubEndDate"},
                                                                    {data: "PackageType"}

                                                                ],

                                                            });
                                                        }
                                                        function ApplyFilters() {

                                                            $('#hdnGroupId').val($('#ddlGroup').val());
                                                            $('#hdnCountry').val($('#ddlCountry').val());
                                                            var table = $('#tblStockReport').DataTable();
                                                            table.destroy();
                                                            table.clear();
                                                            loadTable();

                                                        }
                                                        function ClearFilters() {
                                                            // $("select").val("None");
                                                            //$("select").not(".disabled").formSelect();
                                                            $("select").val(null).trigger('change');
                                                            $('#hdnCountry').val('');
                                                            $('#hdnGroupId').val('');

                                                            var table = $('#tblStockReport').DataTable();
                                                            table.destroy();
                                                            table.clear();
                                                            loadTable();

                                                        }


                                                        var th = 0;
                                                        var j = 1;
                                                        var today = moment();
                                                        var curDate = today.format('YYYY-MM-DD');
                                                        var curDateTime = today.format('YYYY-MM-DD HH:mm:ss');
                                                        var excel, formatContent, textStyle, isstringStyle, dStyle, zeroStyle;

                                                        function generateajaxcall() {

                                                            var TotalLength = parseInt($('#hdnTotalRecords').val());
                                                            var startLength = parseInt($('#hdnStartCount').val());
                                                            var limit = parseInt($('#hdnLimitExport').val());

                                                            if (TotalLength > 500) {
                                                                var perc = parseInt((limit / TotalLength * 100));
                                                                if (perc > 100) {
                                                                    var percentage = "100%";
                                                                } else {
                                                                    var percentage = perc + "%";
                                                                }
                                                            } else {
                                                                var percentage = "100%";
                                                            }
                                                            $('#divProgress').attr('style', 'width:' + percentage);
                                                            $("#spnProgress").text(percentage);
                                                            $(".overlay").show();
                                                            $("#progress").show();
                                                            if ($('#hdnExportClicked').val() == 'N') {
                                                                th = 0;
                                                                j = 1;

                                                                excel = $JExcel.new("Arial dark 10 #333333");
                                                                excel.set({sheet: 0, value: "Global Stock Report"});
                                                                var headers = ["SL.No", "AmberAuthToken", "IMEI", "SimNumber", "AdminUserName", "Country", "ActiveStatus", "RetailPhone", "AmberModel", "SimProvider", "DeviceType", "UserEmailId", "UserPhone", "PhoneNumber", "RetailName", "AppUserName", "UserMappedDate", "HBTDate", "RetailDate", "UpdatedDate", "FaultDevice", "GroupName", "FleetId", "FleetName", "RetailKyc", "InTransit", "SubscriptionStartDate", "SubscriptionEndDate", "DataPacket", "VehicleName", "PlateNo", "Retail UserName", "SoldBy", "RetailGroupName", "RetailCountry", "PurchaseDate", "Remarks", "Shield Sub Start Date", "Shield Sub End Date", "SubscriptionType"];
                                                                var formatHeader = excel.addStyle({
                                                                    border: "none,none,none,thin #333333",
                                                                    font: "Arial 10 #333333 B",
                                                                    align: "C C"});
                                                                formatContent = excel.addStyle({
                                                                    border: "none,none,none,thin #333333",
                                                                    font: "Arial 11 #333333",
                                                                    align: "C C"});
                                                                zeroStyle = excel.addStyle({
                                                                    border: "none,none,none,thin #333333",
                                                                    font: "Arial 11 #333333",
                                                                    align: "C C",
                                                                    format: '0'});
                                                                dStyle = excel.addStyle({
                                                                    align: "R",
                                                                    format: "yyyy.mm.dd hh:mm:ss",
                                                                    font: "#00AA00"}
                                                                );
                                                                textStyle = excel.addStyle({
                                                                    border: "none,none,none,thin #333333",
                                                                    font: "Arial 11 #333333",
                                                                    align: "C C",
                                                                    format: "@"});
                                                                isstringStyle = excel.addStyle({
                                                                    border: "none,none,none,thin #333333",
                                                                    font: "Arial 11 #333333",
                                                                    align: "C C",
                                                                    format: "@",
                                                                    isstring: true
                                                                });
                                                                for (var i = 0; i < headers.length; i++) {
                                                                    excel.set(0, i, 0, headers[i], formatHeader);
                                                                    excel.set(0, i, undefined, 40);
                                                                }

                                                            }
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "ajax/StockReport" + extension,
                                                                data: {
                                                                    offset: offset,
                                                                    Country: $('#hdnCountry').val(),
                                                                    FilterGroupId: $('#hdnGroupId').val(),
                                                                    start: startLength,
                                                                    length: "100",
                                                                    exportFlag: "Y",
                                                                },
                                                                success: function (data) {

                                                                    $('#hdnExportClicked').val('Y');
                                                                    var parsedData = JSON.parse(data);
                                                                    dataArray = [];
                                                                    if (parsedData.success == 'Y') {
                                                                        dataArray = parsedData.data;

                                                                        if (dataArray.length > 0) {
                                                                            th++;
                                                                            console.log(th);
                                                                            for (var i = 0; i < dataArray.length; i++) {

                                                                                excel.set(0, 0, j, j, formatContent);
                                                                                excel.set(0, 1, j, removeSpecial(dataArray[i]['AmberAuthToken']), textStyle);
                                                                                excel.set(0, 2, j, removeSpecial(dataArray[i]['IMEI']), isstringStyle);
                                                                                excel.set(0, 3, j, removeSpecial(dataArray[i]['SimNumber']), isstringStyle);
                                                                                excel.set(0, 4, j, removeSpecial(dataArray[i]['AdminUserName']), textStyle);
                                                                                excel.set(0, 5, j, removeSpecial(dataArray[i]['Country']), textStyle);
                                                                                excel.set(0, 6, j, removeSpecial(dataArray[i]['ActiveStatus']), textStyle);
                                                                                excel.set(0, 7, j, removeSpecial(dataArray[i]['RetailPhone']), isstringStyle);
                                                                                excel.set(0, 8, j, removeSpecial(dataArray[i]['AmberModel']), textStyle);
                                                                                excel.set(0, 9, j, removeSpecial(dataArray[i]['SimProvider']), textStyle);
                                                                                excel.set(0, 10, j, removeSpecial(dataArray[i]['DeviceType']), textStyle);
                                                                                excel.set(0, 11, j, dataArray[i]['UserEmailId'], textStyle);
                                                                                excel.set(0, 12, j, removeSpecial(dataArray[i]['UserPhone']), isstringStyle);
                                                                                excel.set(0, 13, j, removeSpecial(dataArray[i]['PhoneNumber']), isstringStyle);
                                                                                excel.set(0, 14, j, removeSpecial(dataArray[i]['RetailName']), textStyle);
                                                                                excel.set(0, 15, j, removeSpecial(dataArray[i]['AppUserName']), textStyle);
                                                                                excel.set(0, 16, j, dataArray[i]['UserMappedDate'], textStyle);
                                                                                excel.set(0, 17, j, dataArray[i]['HBTDate'], textStyle);
                                                                                excel.set(0, 18, j, dataArray[i]['RetailDate'], textStyle);
                                                                                excel.set(0, 19, j, dataArray[i]['UpdatedDate'], textStyle);
                                                                                excel.set(0, 20, j, removeSpecial(dataArray[i]['FaultDevice']), textStyle);
                                                                                excel.set(0, 21, j, removeSpecial(dataArray[i]['AdminGroupName']), textStyle);
                                                                                excel.set(0, 22, j, removeSpecial(dataArray[i]['FleetId']), textStyle);
                                                                                excel.set(0, 23, j, removeSpecial(dataArray[i]['FleetName']), textStyle);
                                                                                excel.set(0, 24, j, removeSpecial(dataArray[i]['RetailKyc']), textStyle);
                                                                                excel.set(0, 25, j, removeSpecial(dataArray[i]['InTransit']), textStyle);
                                                                                excel.set(0, 26, j, dataArray[i]['SubscriptionStartDate'], textStyle);
                                                                                excel.set(0, 27, j, dataArray[i]['SubscriptionEndDate'], textStyle);
                                                                                excel.set(0, 28, j, dataArray[i]['DataPacket'], textStyle);
                                                                                excel.set(0, 29, j, dataArray[i]['VehicleName'], textStyle);
                                                                                excel.set(0, 30, j, dataArray[i]['PlateNo'], textStyle);
                                                                                excel.set(0, 31, j, dataArray[i]['RetailUser'], textStyle);
                                                                                excel.set(0, 32, j, dataArray[i]['SoldBy'], textStyle);
                                                                                excel.set(0, 33, j, dataArray[i]['RetailGroupName'], textStyle);
                                                                                excel.set(0, 34, j, dataArray[i]['RetailCountry'], textStyle);
                                                                                excel.set(0, 35, j, dataArray[i]['PurchaseDate'], textStyle);

                                                                                excel.set(0, 36, j, dataArray[i]['Remarks'], textStyle);
                                                                                excel.set(0, 37, j, dataArray[i]['ShieldSubStartDate'], textStyle);
                                                                                excel.set(0, 38, j, dataArray[i]['ShieldSubEndDate'], textStyle);
                                                                                excel.set(0, 39, j, dataArray[i]['PackageType'], textStyle);

                                                                                j++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (TotalLength > limit) {
                                                                        $('#hdnStartCount').val(parseInt($('#hdnStartCount').val()) + 10);
                                                                        $('#hdnLimitExport').val(parseInt($('#hdnLimitExport').val()) + 100);
                                                                        setTimeout(function () {
                                                                            generateajaxcall();
                                                                        }, 3000);
                                                                    } else {
                                                                        $('#hdnStartCount').val("0");
                                                                        $('#hdnLimitExport').val("500");

                                                                        excel.generate("Global-Stock " + curDateTime + ".xlsx");
                                                                        $(".overlay").hide();
                                                                        $("#progress").hide();
                                                                        $('#hdnExportClicked').val('N');
                                                                    }
                                                                }

                                                            });
                                                        }


        </script>
    </body>
</html>