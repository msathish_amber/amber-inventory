<?php

include("../config/config.php");

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];

$loginUrl = $service_domain . "retailer/retailadminlogin";
$loginPostArray = array("UserName" => $username, "Password" => $password, "Type" => "Inventory", "Platform" => "Web_Inventory");
$loginData = getData($loginUrl, $loginPostArray);
$loginJson = json_decode($loginData);

$loginSuccess = $loginJson->success;

$lastRecordDate = $loginJson->EmergencyAlertLastDate;

$serverTime = $loginJson->ServerCurrentTime;

$GMTtime = gmdate("Y-m-d H:i:s", mktime(date("h"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$NewTimeCalc = strtotime($GMTtime) + $offset;
$CalcServerTime = $NewTimeCalc - strtotime($serverTime);

if ($loginSuccess == 'Y') {
    $_SESSION['Amber_Inventory_UserToken'] = $loginJson->UserToken;
    $_SESSION['Amber_Inventory_UserName'] = $loginJson->Name;
    $_SESSION['Amber_Inventory_AdminFlag'] = $loginJson->AdminFlag;
    $_SESSION['Amber_Inventory_SuperAdminFlag'] = $loginJson->SuperAdminFlag;
    $_SESSION['Amber_Inventory_Id'] = $loginJson->Id;
    $_SESSION['Amber_Inventory_AmberAuthToken'] = $loginJson->AmberAuthToken;
    $_SESSION['Amber_Inventory_GroupId'] = $loginJson->GroupId;
    $_SESSION['Amber_Inventory_HelpMeGroupId'] = $loginJson->HelpMeGroupId;
    $screenArray = json_decode($loginJson->Screens);
    $_SESSION['Amber_Inventory_Screen_Dashboard'] = $screenArray->DASHBOARD;
    $_SESSION['Amber_Inventory_Screen_StockReport'] = $screenArray->STOCKREPORT;
    $_SESSION['Amber_Inventory_Screen_SubscriptionReport'] = $screenArray->SUBSCRIPTIONREPORT;
    $_SESSION['Amber_Inventory_Screen_DeviceActivation'] = $screenArray->DEVICEACTIVATION;
    $_SESSION['Amber_Inventory_Screen_ShieldActivation'] = $screenArray->SHIELDACTIVATION;
    $_SESSION['Amber_Inventory_Screen_DeviceRenewal'] = $screenArray->DEVICERENEWAL;
    $_SESSION['Amber_Inventory_Screen_ShieldRenewal'] = $screenArray->SHIELDRENEWAL;
    $_SESSION['Amber_Inventory_Screen_MonthlyBilling'] = $screenArray->MONTHLYBILLING;
    $_SESSION['Amber_Inventory_Screen_UnitNotWorking'] = $screenArray->UNITSNOTWORKING;
    $_SESSION['Amber_Inventory_Screen_OpenApi'] = $screenArray->OPENAPI;
    $_SESSION['Amber_Inventory_Screen_HelpMeActivation'] = $screenArray->HELPMEACTIVATION;
    $_SESSION['Amber_Inventory_Screen_HelpMeRenewal'] = $screenArray->HELPMERENEWAL;
    $_SESSION['Amber_Inventory_Screen_HelpMeUsers'] = $screenArray->HELPMEUSERS;
    $_SESSION['Amber_Inventory_Screen_UserView'] = $screenArray->USERS->VIEW;
    $_SESSION['Amber_Inventory_Screen_UserCreate'] = $screenArray->USERS->CREATE;
    $_SESSION['Amber_Inventory_Screen_UserEdit'] = $screenArray->USERS->EDIT;
    $_SESSION['Amber_Inventory_Screen_UserDelete'] = $screenArray->USERS->DELETE;
    $_SESSION['Amber_Inventory_Screen_UserReset'] = $screenArray->USERS->RESET_PASSWORD;


    if ($_SESSION['Amber_Inventory_Screen_Dashboard'] == 'Y') {
        $LOGINPAGE = 'dashboard';
    } else if ($_SESSION['Amber_Inventory_Screen_StockReport'] == 'Y') {
        $LOGINPAGE = 'globalstockreport';
    } else if ($_SESSION['Amber_Inventory_Screen_SubscriptionReport'] == 'Y') {
        $LOGINPAGE = 'subscriptionforecasting';
    } else if ($_SESSION['Amber_Inventory_Screen_DeviceActivation'] == 'Y') {
        $LOGINPAGE = 'deviceactivation';
    } else if ($_SESSION['Amber_Inventory_Screen_ShieldActivation'] == 'Y') {
        $LOGINPAGE = 'shieldactivation';
    } else if ($_SESSION['Amber_Inventory_Screen_DeviceRenewal'] == 'Y') {
        $LOGINPAGE = 'devicerenewal';
    } else if ($_SESSION['Amber_Inventory_Screen_ShieldRenewal'] == 'Y') {
        $LOGINPAGE = 'shieldrenewal';
    } else if ($_SESSION['Amber_Inventory_Screen_MonthlyBilling'] == 'Y') {
        $LOGINPAGE = 'monthlyrenewal';
    } else if ($_SESSION['Amber_Inventory_Screen_UnitNotWorking'] == 'Y') {
        $LOGINPAGE = 'unitsrenewal';
    } else if ($_SESSION['Amber_Inventory_Screen_UserView'] == 'Y') {
        $LOGINPAGE = 'users';
    }
}

if (!empty($_SESSION['Amber_Inventory_UserToken'])) {
    echo "Y~~" . $LOGINPAGE;
} else {
    echo "N";
}

if (!empty($lastRecordDate)) {
    $_SESSION['Amber_DT_notify'] = $lastRecordDate;
} else {
    $_SESSION['Amber_DT_notify'] = $serverTime;
}
?>
