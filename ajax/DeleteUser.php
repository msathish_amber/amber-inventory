<?php

include("../config/config.php");

$UserToken = $_REQUEST['UserToken'];

$RequestArray = array("UserToken" => $UserToken);

$RequestUrl = $service_domain . "inventory/deleteuser";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultJson = json_decode($ResultData);
$ResponseData = $ResultJson->success;
if ($ResponseData == 'Y') {
    echo $ResponseData;
} else {
    $ResponseData = $ResultJson->error;
    echo $ResponseData;
}
?>