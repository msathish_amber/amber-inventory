<?php

include("../config/config.php");

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];

$loginUrl = $service_domain . "inventory/optvalidation";
$loginPostArray = array("UserName" => $username, "Type" => "Inventory", "Platform" => "Web_Inventory");
$loginData = getData($loginUrl, $loginPostArray);
$loginJson = json_decode($loginData);

$loginSuccess = $loginJson->success;

$EmailId = $loginJson->Email;

$serverTime = $loginJson->ServerCurrentTime;

if (!empty($EmailId)) {
    echo $EmailId;
} else {
    echo "N";
}

?>
