<?php

include("../config/config.php");

$UserToken = $_REQUEST['UserToken'];
$Password = $_REQUEST['userPassword'];
$RequestArray = array("UserToken" => $UserToken, "Password" => $Password);
$RequestUrl = $service_domain . "inventory/resetpassword";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultJson = json_decode($ResultData);
$ResponseData = $ResultJson->success;
if ($ResponseData == 'Y') {
    echo $ResponseData;
} else {
    $ResponseData = $ResultJson->error;
    echo $ResponseData;
}
?>