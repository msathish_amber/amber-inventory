<?php

include("../config/config.php");

$DeviceOffset = $_REQUEST['offset'];
$Country = $_REQUEST['Country'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];
$FilterUserToken = $_REQUEST['FilterUserToken'];
$ActivationType = $_REQUEST['ActivationType'];
$FilterType = $_REQUEST['FilterType'];
$RequestArray = array("DeviceOffset" => 0, "Country" => $Country, "FilterGroupId" => $FilterGroupId,"CustomStartDate"=>$CustomStartDate,"CustomEndDate"=>$CustomEndDate);
$PostUrl = $service_domain . "inventory/stockreportcount";
$postArray = array_merge($RequestArray, $commonPostArray);
$ResultAray = array();
$ResultData = getData($PostUrl, $postArray);
$ResultJson = json_decode($ResultData);
$Data = $ResultJson->GroupStatistics;

if ($FilterType == 'Month') {
    $checked = "checked=checked";
} else {
    $checked = "";
}
echo "Y~~";
foreach ($Data as $val):
    $GroupData[] = $val->GroupName;
    $TotalData[] = $val->TotalStock;
    $SoldData[] = $val->Sold;
    $InstockData[] = $val->InStock;
    $B2BData[] = $val->B2B;
    $B2cData[] = $val->B2C;
    $BlankData[] = $val->Blank;
endforeach;

echo json_encode($GroupData) . '~~';
echo json_encode($TotalData) . '~~';
echo json_encode($SoldData) . '~~';
echo json_encode($InstockData) . '~~';
echo json_encode($B2BData) . '~~';
echo json_encode($B2cData) . '~~';
echo json_encode($BlankData);

?>