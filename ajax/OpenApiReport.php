<?php

include("../config/config.php");
$ExportFlag = $_REQUEST['exportflag'];
$draw = $_REQUEST['draw'];
$start = $_REQUEST['start'];
if ($start > 0) {
    $start = ($start / 10) + 1;
}

$limit = $_REQUEST['length'];

$DeviceOffset = $_REQUEST['offset'];
$UsageYear = $_REQUEST['UsageYear'];
$UsageMonth = $_REQUEST['UsageMonth'];

$Country = $_REQUEST['Country'];
$FilterGroupId = $_REQUEST['FilterGroupId'];

$RequestArray = array("DeviceOffset" => $DeviceOffset, "Page" => $start,
    "Limit" => $limit, "draw" => $draw, "FilterGroupId" => $FilterGroupId,"Country" => $Country,
    "UsageMonth" => $UsageMonth, "UsageYear" => $UsageYear);
$RequestUrl = $service_domain . "inventory/openapireport";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultAray = array();
$ResultAray = $ResultData;
echo $ResultAray;
?>