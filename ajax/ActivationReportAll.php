<?php

include("../config/config.php");

$DeviceOffset = $_REQUEST['offset'];
$Country = $_REQUEST['Country'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];
$FilterUserToken = $_REQUEST['FilterUserToken'];
$FilterType = $_REQUEST['FilterType'];
$ActivationType = "DEVICE";
$RequestArray = array("DeviceOffset" => 0, "Country" => $Country, "ActivationType" => $ActivationType, "CustomStartDate" => $CustomStartDate, "CustomEndDate" => $CustomEndDate, "FilterGroupId" => $FilterGroupId, "FilterType" => $FilterType);
$PostUrl = $service_domain . "inventory/activationreports";
$postArray = array_merge($RequestArray, $commonPostArray);
$ResultAray = array();
$ResultData = getData($PostUrl, $postArray);
$ResultJson = json_decode($ResultData);
$Data = $ResultJson->Statistics;
$GroupData = $ResultJson->GroupStatistics;
$TotalCount = $ResultJson->TotalCount;
if ($FilterType == 'Month') {
    $checked = "checked=checked";
} else {
    $checked = "";
}
echo "Y~~";

foreach ($Data as $val):
    $month = "";
    $dt = DateTime::createFromFormat('!m', $val->month);
    $month = $dt->format('F');
    if ($FilterType == "Day") {
        $time = strtotime($val->date);
        $newformat = date('M-d', $time);
        $DataDetails[] = $newformat;
    } else {
        $DataDetails[] = $month;
    }
    $DataCount[] = $val->count;
endforeach;

echo "~~";
echo json_encode($DataDetails) . "~~";
echo json_encode($DataCount) . "~~";


?>
