<?php
include("../config/config.php");

$DeviceOffset = $_REQUEST['offset'];
$Country = $_REQUEST['Country'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];

$SubscriptionType = $_REQUEST['SubscriptionType'];
$FilterType = $_REQUEST['FilterType'];
$PackageType = $_REQUEST['PackageType'];

$RequestArray = array("DeviceOffset" => 0, "Country" => $Country, "FilterGroupId" => $FilterGroupId,
    "SubscriptionType" => $SubscriptionType, "CustomStartDate" => $CustomStartDate,
    "CustomEndDate" => $CustomEndDate, "FilterType" => $FilterType, "PackageType" => $PackageType,
    "HelpMeGroupId" => $_SESSION['Amber_Inventory_HelpMeGroupId']);

$PostUrl = $service_domain . "inventory/helpmereportcount";
$postArray = array_merge($RequestArray, $commonPostArray);
$ResultAray = array();
$ResultData = getData($PostUrl, $postArray);
$ResultJson = json_decode($ResultData);
$Data = $ResultJson->Statistics;
$GroupData = $ResultJson->GroupStatistics;
$PackageData = $ResultJson->PackageArray;
foreach ($PackageData as $val):
    if (in_array($val->PackageName, $PackDetails)) {
        $key = array_search($val->PackageName, $PackDetails);
        $PackCount[$key] += $val->count;
    } else {
        $PackDetails[] = $val->PackageName;
        $PackCount[] = $val->count;
    }
endforeach;
$TotalCount = $ResultJson->TotalCount;
if ($FilterType == 'Month') {
    $checked = "checked=checked";
} else {
    $checked = "";
}
echo "Y~~";
?>
<div class="card-move-up waves-effect waves-block waves-light">
    <div class="move-up cyan darken-1" style="    background: linear-gradient(45deg, #ff6f00, #ffca28) !important;">
        <div>
            <div class="switch chart-revenue-switch ">
                <label class="cyan-text white-text">
                    Day <input type="checkbox" <?php echo $checked; ?> onchange="FilterTypeChange(this)"> <span class="lever"></span> Month
                </label>
                <a class="waves-effect waves-light btn gradient-45deg-purple-deep-orange gradient-shadow right activator acti" onclick="loadgrouptable();" >Details</a>
            </div>

        </div>
        <div class="trending-line-chart-wrapper"><canvas id="revenue-line-chart" height="70"></canvas></div>
    </div>
</div>
<div class="card-content right-hide-list" style="margin-top: 15px;">
    <a class="btn-floating btn-move-up waves-effect waves-light red accent-2 z-depth-4 right" >
        <i class="material-icons activator acti" id="loadgrouptable" onclick="loadgrouptable();">filter_list</i>
    </a>
    <div class="col s12 m3 l3">
        <div id="doughnut-chart-wrapper" style="margin-top: 10%;">
            <div id="pie-chart-sample" class="center" ></div>

        </div>
    </div>
    <div class="col s12 m2 l2">
        <ul class="doughnut-chart-legend" style="margin-top: 45%;">
            <?php if (count($PackDetails) == 3) { ?>
                <li class="Second ultra-small"><span class="legend-color"></span><?php echo $PackDetails[0] . ' - ' . $PackCount[0]; ?></li>
                <li class="First ultra-small"><span class="legend-color"></span><?php echo $PackDetails[1] . ' - ' . $PackCount[1]; ?></li>
                <li class="Third ultra-small"><span class="legend-color"></span><?php echo $PackDetails[2] . ' - ' . $PackCount[2]; ?></li>
            <?php } ?>
            <?php if (count($PackDetails) == 2) { ?>
                <li class="Second ultra-small"><span class="legend-color"></span><?php echo $PackDetails[0] . ' - ' . $PackCount[0]; ?></li>
                <li class="First ultra-small"><span class="legend-color"></span><?php echo $PackDetails[1] . ' - ' . $PackCount[1]; ?></li>

            <?php } ?>
            <?php if (count($PackDetails) == 1) { ?>
                <li class="Second ultra-small"><span class="legend-color"></span><?php echo $PackDetails[0] . ' - ' . $PackCount[0]; ?></li>

            <?php } ?>
        </ul>
    </div>
    <div class="col s12 m5 l6">
        <div class="trending-bar-chart-wrapper"><canvas id="trending-bar-chart" ></canvas></div>
    </div>
</div>
<div class="card-reveal">
    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i> 
    </span>
    <table class="responsive-table" id="tblActivation">
        <thead>
            <tr>
                <th>User Name</th>
                <th>User Email</th>
                <th>User Phone</th>                                                   
                <th>Subscription From</th>
                <th>Subscription To</th>
                <th>Subscription Type</th>
                <th>Admin GroupName</th>
                <th>Package Name</th>
                <th>Activated Date</th>
                <th>Created Date</th>
                <th>Days</th>
                <th>Member Type</th>
                <th>Linked Member</th>
                <th>Monthly Subscription</th>
                <th>Sale By</th>
                <th>Client Group</th>                                                   
                <th>Campaign/Source</th>
              
                <th>Promo Code</th>
                <th>Id Number</th>  
                <th>Address1</th> 
                <th>Address2</th> 
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>


<?php
foreach ($Data as $val):
    $month = "";
    $dt = DateTime::createFromFormat('!m', $val->month);
    $month = $dt->format('F');
    if ($FilterType == "Day") {
        $time = strtotime($val->date);
        $newformat = date('M-d', $time);
        $DataDetails[] = $newformat;
    } else {
        $DataDetails[] = $month;
    }
    $DataCount[] = $val->count;
endforeach;

foreach ($GroupData as $val):
    if (in_array($val->Group, $GroupDetails)) {
        $key = array_search($val->Group, $GroupDetails);
        $GroupCount[$key] += $val->count;
    } else {
        $GroupDetails[] = $val->Group;
        $GroupCount[] = $val->count;
    }
endforeach;

echo "~~";
echo json_encode($DataDetails) . "~~";
echo json_encode($DataCount) . "~~";
echo json_encode($GroupDetails) . "~~";
echo json_encode($GroupCount) . "~~";
echo json_encode($PackCount);
?>
