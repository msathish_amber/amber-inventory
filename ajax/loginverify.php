<?php

include("../config/config.php");

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
$curDateTime = $_REQUEST['curDateTime'];
$Offset = $_REQUEST['offset'];

$loginUrl = $service_domain . "inventory/adminlogin";
$loginPostArray = array("UserName" => $username, "Password" => $password, "Type" => "Inventory", "Platform" => "Web_Inventory","DeviceOffset"=>$Offset,"DeviceTime"=>$curDateTime);
$loginData = getData($loginUrl, $loginPostArray);
$loginJson = json_decode($loginData);

$loginSuccess = $loginJson->success;

$lastRecordDate = $loginJson->EmergencyAlertLastDate;

$serverTime = $loginJson->ServerCurrentTime;

$GMTtime = gmdate("Y-m-d H:i:s", mktime(date("h"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$NewTimeCalc = strtotime($GMTtime) + $offset;
$CalcServerTime = $NewTimeCalc - strtotime($serverTime);

if ($loginSuccess == 'Y') {
    $_SESSION['Amber_Inventory_UserToken'] = $loginJson->UserToken;
    $_SESSION['Amber_Inventory_UserName'] = $loginJson->Name;
    $_SESSION['Amber_Inventory_AdminFlag'] = $loginJson->AdminFlag;
    $_SESSION['Amber_Inventory_Id'] = $loginJson->Id;
    $_SESSION['Amber_Inventory_AmberAuthToken'] = $loginJson->AmberAuthToken;
    $_SESSION['Amber_Inventory_GroupId'] = $loginJson->GroupId;   
}

if (!empty($_SESSION['Amber_Inventory_UserToken'])) {
    echo "Y";
} else {
    echo "N";
}

if (!empty($lastRecordDate)) {
    $_SESSION['Amber_DT_notify'] = $lastRecordDate;
} else {
    $_SESSION['Amber_DT_notify'] = $serverTime;
}
?>
