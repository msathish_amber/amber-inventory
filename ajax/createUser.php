<?php

include("../config/config.php");
$Name = $_REQUEST['name'];
$UserName = $_REQUEST['userName'];
$groupUser = $_REQUEST['groupUser'];
$userEmail = $_REQUEST['userEmail'];
$userPassword = $_REQUEST['userPassword'];
$screenname = $_POST['screenname'];
$screenvalue = $_POST['screenvalue'];
$screenaccess = array_combine($screenname, $screenvalue);

$screenusername = $_POST['screenusername'];
$screenuservalue = $_POST['screenuservalue'];
$screenuseraccess = array_combine($screenusername, $screenuservalue);
$ScreenUser = array("USERS" => $screenuseraccess);

$Screens = array_merge($screenaccess, $ScreenUser);

$AdminFlag = $_REQUEST['AdminFlag'];
$GroupAdminFlag = $_REQUEST['GroupAdminFlag'];
$SuperAdminFlag = $_REQUEST['SuperAdminFlag'];


$RequestArray = array("Name" => $Name, "UserName" => $UserName, "GroupId" => $groupUser, "Email" => $userEmail, "Password" => $userPassword, "AdminFlag" => $AdminFlag, "GroupAdminFlag" => $GroupAdminFlag, "SuperAdminFlag" => $SuperAdminFlag, "Screens" => json_encode($Screens));
$RequestUrl = $service_domain . "inventory/userregister";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultJson = json_decode($ResultData);
$ResponseData = $ResultJson->success;
if ($ResponseData == 'Y') {
    echo $ResponseData;
} else {
    $ResponseData = $ResultJson->error;
    echo $ResponseData;
}
?>