<?php

include("../config/config.php");

$UserToken = $_REQUEST['UserToken'];
$UserName = $_REQUEST['UserName'];
$Name = $_REQUEST['Name'];
$Email = $_REQUEST['Email'];
$GroupId = $_REQUEST['groupUser'];
$screenname = $_POST['screenname'];
$screenvalue = $_POST['screenvalue'];
$screenaccess = array_combine($screenname, $screenvalue);

$screenusername = $_POST['screenusername'];
$screenuservalue = $_POST['screenuservalue'];
$screenuseraccess = array_combine($screenusername, $screenuservalue);
$ScreenUser = array("USERS" => $screenuseraccess);

$Screens = array_merge($screenaccess, $ScreenUser);

$AdminFlag = $_REQUEST['AdminFlag'];
$GroupAdminFlag = $_REQUEST['GroupAdminFlag'];
$SuperAdminFlag = $_REQUEST['SuperAdminFlag'];

$RequestArray = array("UserToken" => $UserToken, "UserName" => $UserName, "Name" => $Name, "Email" => $Email, "GroupId" => $GroupId, "AdminFlag" => $AdminFlag, "GroupAdminFlag" => $GroupAdminFlag, "SuperAdminFlag" => $SuperAdminFlag, "Screens" => json_encode($Screens));
$RequestUrl = $service_domain . "inventory/edituser";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultJson = json_decode($ResultData);
$ResponseData = $ResultJson->success;
if ($ResponseData == 'Y') {
    echo $ResponseData;
} else {
    $ResponseData = $ResultJson->error;
    echo $ResponseData;
}
?>