<?php

include("../config/config.php");
$ExportFlag = $_REQUEST['exportflag'];
$draw = $_REQUEST['draw'];
$start = $_REQUEST['start'];
if ($start > 0) {
    $start = ($start / 10) + 1;
}
$limit = $_REQUEST['length'];
$SearchKeyword = $_REQUEST['search']['value'];
$Country = $_REQUEST['Country'];
$DeviceOffset = $_REQUEST['offset'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];
$FilterUserToken = $_REQUEST['FilterUserToken'];
$FilterType = $_REQUEST['FilterType'];
$FleetFlag = $_REQUEST['FleetFlag'];
$RenewalType = $_REQUEST['RenewalType'];
$SubscriptionType = $_REQUEST['SubscriptionType'];
$RequestArray = array("ExportFlag" => $ExportFlag, "FleetFlag" => $FleetFlag, "DeviceOffset" => 0, "Page" => $start, "Limit" => $limit, "Country" => $Country, "ActivationType" => $SubscriptionType, "CustomStartDate" => $CustomStartDate, "CustomEndDate" => $CustomEndDate, "FilterGroupId" => $FilterGroupId, "draw" => $draw, "FilterType" => $FilterType, "RenewalType" => $RenewalType);
$RequestUrl = $service_domain . "inventory/monthlyrenewalreport";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultAray = array();
$ResultAray = $ResultData;
echo $ResultAray;
?>