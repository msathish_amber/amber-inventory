<?php
include("../config/config.php");

$DeviceOffset = $_REQUEST['offset'];
$Country = $_REQUEST['Country'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];
$FilterUserToken = $_REQUEST['FilterUserToken'];
$ActivationType = $_REQUEST['ActivationType'];
$FilterType = $_REQUEST['FilterType'];
$RequestArray = array("DeviceOffset" => 0, "Country" => $Country, "ActivationType" => $ActivationType, "CustomStartDate" => $CustomStartDate, "CustomEndDate" => $CustomEndDate, "FilterGroupId" => $FilterGroupId, "FilterType" => $FilterType);
$PostUrl = $service_domain . "inventory/activationreports";
$postArray = array_merge($RequestArray, $commonPostArray);
$ResultAray = array();
$ResultData = getData($PostUrl, $postArray);
$ResultJson = json_decode($ResultData);
$Data = $ResultJson->Statistics;
$GroupData = $ResultJson->GroupStatistics;
$TotalCount = $ResultJson->TotalCount;
if ($FilterType == 'Month') {
    $checked = "checked=checked";
} else {
    $checked = "";
}
echo "Y~~";
?>
<div class="card-move-up waves-effect waves-block waves-light">
    <div class="move-up cyan darken-1" style="    background: linear-gradient(45deg, #ff6f00, #ffca28) !important;">
        <div>
            <div class="switch chart-revenue-switch ">
                <label class="cyan-text white-text">
                    Day <input type="checkbox" <?php echo $checked; ?> onchange="FilterTypeChange(this)"> <span class="lever"></span> Month
                </label>
                <a class="waves-effect waves-light btn gradient-45deg-purple-deep-orange gradient-shadow right activator acti" onclick="loadgrouptable();" >Details</a>
            </div>



        </div>
        <div class="trending-line-chart-wrapper"><canvas id="revenue-line-chart" height="70"></canvas></div>
    </div>
</div>
<div class="card-content right-hide-list" style="margin-top: 15px;">
    <a class="btn-floating btn-move-up waves-effect waves-light red accent-2 z-depth-4 right" >
        <i class="material-icons activator acti" id="loadgrouptable" onclick="loadgrouptable();">filter_list</i>
    </a>
    <div class="col s12 m3 l3">
        <div id="doughnut-chart-wrapper" style="margin-top: 10%;">
            <div id="pie-chart-sample" class="center" ></div>

        </div>
    </div>
    <div class="col s12 m2 l2">
        <ul class="doughnut-chart-legend" style="margin-top: 45%;">
            <li class="Second ultra-small"><span class="legend-color"></span>B2B - <?php echo $ResultJson->B2B; ?></li>
            <li class="First ultra-small"><span class="legend-color"></span>B2C - <?php echo $ResultJson->B2C; ?></li>
        </ul>
    </div>
    <div class="col s12 m5 l6">
        <div class="trending-bar-chart-wrapper"><canvas id="trending-bar-chart" ></canvas></div>
    </div>
</div>
<div class="card-reveal">
    <span class="card-title grey-text text-darken-4">Activation List  <i class="material-icons right">close</i>
    </span>
    <table class="responsive-table" id="tblActivation">
        <thead>
            <tr>
                <th>AmberAuthToken</th>
                <th>IMEI</th>
                <th>Amber Model</th>
                <th>Country</th>
                <th>Group Name</th>
                <th>Fleet Flag</th>
                <th>Device Type</th>
                <th>User EmailId</th>
                <th>User Phone</th>
                <th>Retail Name</th>
                <th>App UserName</th>
                <th>Days</th>
                <th>Retail Kyc Status</th>
                <th>Retail User</th>
                <th>Sold By</th>
                <th>Retail GroupName</th>
                <th>Retail Country</th>
                <th>Purchase Date</th>                
                <th>User Mapped Date</th>
                <th>Sub Start Date</th>
                <th>Sub End Date</th>
                <th>Retail Date</th>
                <th>Device Activated Date</th>
                <th>Billing Type</th>
                <th>Notes</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>


<?php
foreach ($Data as $val):
    $month = "";
    $dt = DateTime::createFromFormat('!m', $val->month);
    $month = $dt->format('F');
    if ($FilterType == "Day") {
        $time = strtotime($val->date);
        $newformat = date('M-d', $time);
        $DataDetails[] = $newformat;
    } else {
        $DataDetails[] = $month;
    }
    $DataCount[] = $val->count;
endforeach;

foreach ($GroupData as $val):
    if (in_array($val->Group, $GroupDetails)) {
        $key = array_search($val->Group, $GroupDetails);
        $GroupCount[$key] += $val->count;
    } else {
        $GroupDetails[] = $val->Group;
        $GroupCount[] = $val->count;
    }
endforeach;
$FleetType = [$ResultJson->B2B, $ResultJson->B2C];
echo "~~";
echo json_encode($DataDetails) . "~~";
echo json_encode($DataCount) . "~~";
echo json_encode($GroupDetails) . "~~";
echo json_encode($GroupCount) . "~~";
echo json_encode($FleetType);
?>
