<?php

include("../config/config.php");

$draw = $_REQUEST['draw'];
$start = $_REQUEST['start'];
if ($start > 0) {
    $start = ($start / 10) + 1;
}
$length = $_REQUEST['length'];
$SearchKeyword = $_REQUEST['search']['value'];
$DeviceOffset = $_REQUEST['offset'];
$Country = $_REQUEST['Country'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];
$ExportFlag = $_REQUEST['exportFlag'];

$PostArray = array("ExportFlag" => $ExportFlag, "DeviceOffset" => 0, "Country" => $Country, "FilterGroupId" => $FilterGroupId, "AdminUserToken" => $_SESSION['Amber_Inventory_UserToken'], "Page" => $start, "Limit" => $length, "SearchKeyword" => $SearchKeyword, "draw" => $draw);
$usersUrl = $service_domain . "inventory/stockreport";

$postArray = array_merge($commonPostArray, $PostArray);
$ResultAray = array();
$usersData = getData($usersUrl, $postArray);
$ResultAray = $usersData;
echo $ResultAray;
?>