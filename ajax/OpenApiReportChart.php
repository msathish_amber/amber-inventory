<?php
include("../config/config.php");

$DeviceOffset = $_REQUEST['offset'];

$Country = $_REQUEST['Country'];

$FilterGroupId = $_REQUEST['FilterGroupId'];

$UsageYear = $_REQUEST['UsageYear'];
$UsageMonth = $_REQUEST['UsageMonth'];

$RequestArray = array("DeviceOffset" => 0, "UsageYear" => $UsageYear, "Country" => $Country,
    "FilterGroupId" => $FilterGroupId, "UsageMonth" => $UsageMonth);

$PostUrl = $service_domain . "inventory/openapireportcount";
$postArray = array_merge($RequestArray, $commonPostArray);
$ResultAray = array();
$ResultData = getData($PostUrl, $postArray);
$ResultJson = json_decode($ResultData);


$GroupData = $ResultJson->GroupStatistics;
$TotalCount = $ResultJson->TotalCount;

if ($FilterType == 'Month') {
    $checked = "checked=checked";
} else {
    $checked = "";
}

echo "Y~~"
?>
<div class="card-move-up waves-effect waves-block waves-light">
    <div class="move-up cyan darken-1" style="    background: linear-gradient(45deg, #ff6f00, #ffca28) !important;">
        <div>
            <div class="switch chart-revenue-switch ">

                <a class="waves-effect waves-light btn gradient-45deg-purple-deep-orange gradient-shadow right activator acti" onclick="LoadDataTable();" >Details</a>
            </div>
        </div>
        <div class="trending-bar-chart-wrapper"><canvas id="trending-bar-chart" height="100"></canvas></div>
    </div>
</div>
<div class="card-content right-hide-list datatab" >
    <a class="btn-floating btn-move-up waves-effect waves-light red accent-2 z-depth-4 right datatab" >
        <i class="material-icons activator " id="loadtable" onclick="LoadDataTable();">filter_list</i>
    </a>
    <div class="col s12 m3 l3">

    </div>
    <div class="col s12 m2 l2">

    </div>
    <div class="col s12 m5 l6">

    </div>
</div>
<div class="card-reveal">
    <span class="card-title grey-text text-darken-4">Open API Usage Report<i class="material-icons right">close</i>
    </span>
    <table class="responsive-table" id="tblOpenApi">
        <thead>
            <tr>
                <th>CompanyName</th>
                <th>Email</th>
                <th>Country</th>
                <th>Device Count</th>
                <th>APIHits</th>
                <th>APIkey</th>
                <th>Month</th>
                <th>MonthName</th>
                <th>Year</th>
                <th>GroupName</th>
            </tr>
        </thead>
        <tbody id="tblbodyStock">

        </tbody>
    </table>
</div>

<?php
foreach ($GroupData as $val):
    if (in_array($val->Group, $GroupDetails)) {
        $key = array_search($val->Group, $GroupDetails);
        $GroupCount[$key] += $val->count;
    } else {
        $GroupDetails[] = $val->Group;
        $GroupCount[] = $val->count;
    }
endforeach;

echo "~~";
echo json_encode($GroupDetails) . "~~";
echo json_encode($GroupCount) . "~~";
?>
