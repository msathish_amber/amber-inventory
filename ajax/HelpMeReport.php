<?php

include("../config/config.php");

$ExportFlag = $_REQUEST['exportflag'];
$DeviceOffset = $_REQUEST['offset'];
$draw = $_REQUEST['draw'];
$start = $_REQUEST['start'];
if ($start > 0) {
    $start = ($start / 10) + 1;
}
$limit = $_REQUEST['length'];
$SearchKeyword = $_REQUEST['search']['value'];
$DeviceOffset = $_REQUEST['offset'];
$Country = $_REQUEST['Country'];
$CustomStartDate = $_REQUEST['CustomStartDate'];
$CustomEndDate = $_REQUEST['CustomEndDate'];
$FilterGroupId = $_REQUEST['FilterGroupId'];
$SubscriptionType = $_REQUEST['SubscriptionType'];
$PackageType = $_REQUEST['PackageType'];
$FilterType = $_REQUEST['FilterType'];

$RequestArray = array("ExportFlag" => $ExportFlag, "DeviceOffset" => 0, "Page" => $start,
    "Limit" => $limit, "draw" => $draw, "PackageType" => $PackageType, "SubscriptionType" => $SubscriptionType,
    "DeviceOffset" => $DeviceOffset, "FilterType" => $FilterType,"Country" => $Country, 
    "FilterGroupId" => $FilterGroupId,"CustomStartDate" => $CustomStartDate,
    "CustomEndDate" => $CustomEndDate,"HelpMeGroupId"=>$_SESSION['Amber_Inventory_HelpMeGroupId']
    );

$RequestUrl = $service_domain . "inventory/helpmereport";
$PostArray = array_merge($RequestArray, $commonPostArray);
$ResultData = getData($RequestUrl, $PostArray);
$ResultAray = array();
$ResultAray = $ResultData;
echo $ResultAray;
?>