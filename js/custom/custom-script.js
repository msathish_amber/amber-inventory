var ext = window.location.pathname.split('.').pop();
var extension;
if ($.trim(ext) == 'php') {
    extension = ".php";
} else {
    extension = "";
}

//$(window).on('load', serverMaintenance);
//
//function serverMaintenance(){
//    setInterval(function() {
//        $('.header-maintenance').fadeIn(2000).delay(15000).fadeOut(2000);
//    });
//}

var d = new Date()
var offset = -d.getTimezoneOffset() * 60;

function removeSpecial(str) {
    if (str != "" && str != null) {

        str = msgAfterClearEmojis(str);
        str = str.replace(/[&\\\#,+()$~%.'":*?<>{}]/g, '');
    }
    return str;
}
function removeEmojis(string) {
    var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
    return string.replace(regex, '');
}
function msgAfterClearEmojis(sss)
{
   
    var regex = /([#0-9]\u20E3)|[\xA9\xAE\u203C\u2047-\u2049\u2122\u2139\u3030\u303D\u3297\u3299][\uFE00-\uFEFF]?|[\u2190-\u21FF][\uFE00-\uFEFF]?|[\u2300-\u23FF][\uFE00-\uFEFF]?|[\u2460-\u24FF][\uFE00-\uFEFF]?|[\u25A0-\u25FF][\uFE00-\uFEFF]?|[\u2600-\u27BF][\uFE00-\uFEFF]?|[\u2900-\u297F][\uFE00-\uFEFF]?|[\u2B00-\u2BF0][\uFE00-\uFEFF]?|(?:\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDEFF])[\uFE00-\uFEFF]?|[\u20E3]|[\u26A0-\u3000]|\uD83E[\udd00-\uddff]|[\u00A0-\u269F]/g;
    var new_msg = sss.toString().replace(regex, '').trim();
    return new_msg;
}



$('.btn-timer').click(function () {
    swal({
        title: "Coming Soon!",
        text: "Webpage under development",
        timer: 2000,
        buttons: false
    });
});