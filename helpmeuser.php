<?php
include("config/config.php");
if (empty($_SESSION['Amber_Inventory_UserToken']) && $_SESSION['Amber_Inventory_Screen_HelpMeUsers'] != 'Y') {
    header("Location: index" . $extension);
}
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>Amber :: Inventory</title>

        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
        <!-- BEGIN: VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/dashboard.css">
        <link rel="stylesheet" type="text/css" href="css/pages/data-tables.css">
        <link href="css/daterangepicker.css" rel="stylesheet" type="text/css" />
        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <!-- END: Custom CSS-->
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="vendors/sparkline/jquery.sparkline.min.js"></script>
        <script src="vendors/chartjs/chart.min.js"></script>
        <script src="vendors/jquery-jvectormap/jquery-jvectormap.min.js"></script>
        <script src="vendors/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>
        <link href="css/daterangepicker.css" rel="stylesheet" type="text/css"/>
        <style>
            #chart-dashboard .card{
                min-height:600px;
            }
            .btnsmall{
                width:20%;
            }
            .daterangepicker.dropdown-menu{
                display:none;
            }
            .daterangepicker .calendar td {
                font-weight: normal;
            }
            .table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th {
                padding: 5px;
            }
            .table-condensed>thead>tr>th>select {
                display: inherit !important;
            }
            .select-wrapper>ul{
                max-height: 200px !important;
            }
        </style>
    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        <!-- BEGIN: Header-->
        <?php include("header.php"); ?>
        <!-- END: Header-->

        <!-- BEGIN: Page Main-->
        <div id="main">
            <div class="row">
                <div class="col s12">
                    <div class="container">
                        <div id="chart-dashboard">
                            <div class="row">
                                <div class="overlay">
                                </div>
                                <div class="div col s5 offset-s3" id="progress" style=" position: fixed; top: 50%;z-index: 99999; background: white;">
                                    <span>    Downloading Progress</span><span id="spnProgress" style="float:right;">0%</span>
                                    <div class="progress">

                                        <div class="determinate" id="divProgress" ></div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l6">
                                    <h5 class="breadcrumbs-title">HelpMe! Users</h5>

                                </div>
                                <div class="col s12 m6 l6 right-align-md" style="margin-top: 10px;">
                                    <button id="picker" class="waves-effect waves-light  btn gradient-45deg-amber-amber box-shadow-none border-round mr-1 mb-1"><i class="material-icons" >date_range</i></button>
                                </div>


                                <div class="col s12 m9 ">
                                    <div class="card " id="divActivationReport" style="padding-left: 10px;padding-right: 10px;">
                                        <table  id="tblActivation" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <th>User Name</th>
                                                    <th>User Email</th>
                                                    <th>User Phone</th>                                                   
                                                    <th>Subscription From</th>
                                                    <th>Subscription To</th>
                                                    <th>Subscription Type</th>
                                                    <th>Admin GroupName</th>
                                                    <th>Package Name</th>
                                                    <th>Activated Date</th>
                                                    <th>Created Date</th>
                                                    <th>Days</th>
                                                    <th>Member Type</th>
                                                    <th>Linked Member</th>
                                                    <th>Monthly Subscription</th>
                                                    <th>Sale By</th>
                                                    <th>Client Group</th>                                                   
                                                    <th>Campaign/Source</th>
                                                    <th>Promo Code</th>
                                                    <th>Id Number</th>  
                                                    <th>Address1</th> 
                                                    <th>Address2</th> 
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col s12 m3 l3 pr-0 hide-on-med-and-down animate fadeLeft">
                                    <div class="card">
                                        <div class="card-content">
                                            <span class="card-title">Filters</span>
                                            <hr class="p-0 mb-10">

                                            <div class="col s12" id="divGroup">
                                                <label>Filter By Group</label>
                                                <select multiple id="ddlGroup">

                                                    <?php
                                                    $usersGroupUrl = $service_domain . "inventory/listgroups";
                                                    $ReqArray = array("ReportType" => 'HelpMe', "HelpMeGroupId" => $_SESSION['Amber_Inventory_HelpMeGroupId']);
                                                    $PostArray = array_merge($ReqArray, $commonPostArray);
                                                    $usersGroupData = getData($usersGroupUrl, $PostArray);
                                                    $usersGroupJson = json_decode($usersGroupData);
                                                    $usersGroupArray = $usersGroupJson->Items;
                                                    $usersGroupCount = count($usersGroupArray);
                                                    ?>

                                                    <?php
                                                    for ($ci = 0; $ci < $usersGroupCount; $ci++) {
                                                        ?>
                                                        <option value="<?php echo $usersGroupArray[$ci]->GroupId; ?>"><?php echo $usersGroupArray[$ci]->GroupName; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                            </div>

                                            <div class="col s12">
                                                <label>Filter By Package</label>
                                                <select  id="ddlPackType">
                                                    <?php
                                                    $PlanUrl = $service_domain . "user/packagelist";
                                                    $ReqArray = array("Verticle" => 'B2CHELPME');
                                                    $PostArray = array_merge($ReqArray, $commonPostArray);
                                                    $PlanUrlData = getData($PlanUrl, $PostArray);
                                                    $PlanJson = json_decode($PlanUrlData);

                                                    $PlanArray = $PlanJson->List;
                                                    $PlanArrayCount = count($PlanArray);
                                                    ?>


                                                    <option value="">Select Package</option>
                                                    <?php
                                                    for ($ci = 0; $ci < $PlanArrayCount; $ci++) {
                                                        ?>
                                                        <option value="<?php echo $PlanArray[$ci]->PackageName; ?>"><?php echo $PlanArray[$ci]->PackageName; ?></option>
                                                        <?php
                                                    }
                                                    ?>


                                                </select>

                                            </div>


                                            <center> <a class="mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan" style="background: -webkit-linear-gradient(45deg, #0288d1, #26c6da) !important;" onclick="ApplyFilters();">Apply</a>
                                                <a class="mb-6 btn waves-effect waves-light gradient-45deg-red-pink" onclick="ClearFilters();">Clear</a>
                                            </center>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->

        <!-- BEGIN: Footer-->
        <?php include("footer.php"); ?>
        <!-- END: Footer-->
        <input type="hidden" id="hdnCustomStart" name="hdnCustomStart" value="">
        <input type="hidden" id="hdnCustomEnd" name="hdnCustomEnd" value="">
        <input type="hidden" id="hdnGroupId" name="hdnGroupId" value="">
        <input type="hidden" id="hdnCountry" name="hdnCountry" value="">
        <input type="hidden" id="hdnFilterType" name="hdnFilterType" value="Day">
        <input type="hidden" id="hdnDateRange" name="hdnDateRange" value="">
        <input type="hidden" id="hdnTotalRecords" name="hdnTotalRecords" value="">
        <input type="hidden" id="hdnStartCount" name="hdnStartCount" value="0">
        <input type="hidden" id="hdnExportClicked" name="hdnExportClicked" value="N">
        <input type="hidden" id="hdnLimitExport" name="hdnLimitExport" value="500">
        <!-- BEGIN VENDOR JS-->

        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <!-- END PAGE LEVEL JS-->
        <script src="vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
        <script src="js/scripts/data-tables.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script src="//cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="//cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" type="text/javascript"></script>
        <script src="js/daterangepicker.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jszip.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/FileSaver.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/myexcel.js?version=<?php echo $jsVersion; ?>"></script>
        <script>
                                                    $("input[type='radio']").click(function () {
                                                        var radioValue = $("input[name='rbtnFilter']:checked").val();
                                                        if (radioValue == 'Group') {
                                                            $('#divGroup').show();
                                                            $('#divCountry').hide();
                                                        } else {
                                                            $('#divGroup').hide();
                                                            $('#divCountry').show();
                                                        }
                                                        ClearFilters();

                                                    });
                                                    $('#divCountry').hide();
                                                    $('#picker').hide();
                                                    var startdate = moment().subtract(29, 'days').format('YYYY-MM-DD') + " 12:00:00 am";
                                                    var enddate = moment().format('YYYY-MM-DD') + " 11:59:59 pm";
                                                    $('#hdnCustomStart').val(startdate);
                                                    $('#hdnCustomEnd').val(enddate);
                                                    $('#hdnDateRange').val(moment().subtract(29, 'days').format('YYYY-MM-DD') + ' to ' + moment().format('YYYY-MM-DD'));
                                                    $('#picker').html(moment().subtract(29, 'days').format('DD/MM/YYYY') + ' - ' + moment().format('DD/MM/YYYY'));
                                                    $(document).ready(function () {
                                                        $('select').select2({width: "100%"});
                                                        // Get the elements
                                                        $('#picker').daterangepicker({
                                                            linkedCalendars: false,
                                                            format: 'MM/DD/YYYY',
                                                            "showDropdowns": true,
                                                            "alwaysShowCalendars": false,
                                                            "opens": "left",
                                                            cancelClass: "",
                                                            "minDate": moment().subtract(6, 'month'),
                                                            "maxDate": moment(),
                                                            "startDate": moment().subtract(29, 'days'),
                                                            "endDate": moment(),
                                                            "dateLimit": {
                                                                month: 2
                                                            },
                                                            "ranges": {

                                                                "Last 30 Days": [
                                                                    moment().subtract(29, 'days'),
                                                                    moment()
                                                                ],
                                                                "This Month": [
                                                                    moment().startOf('month'),
                                                                    moment()
                                                                ],
                                                                "Last Month": [
                                                                    moment().subtract(1, 'month').startOf('month'),
                                                                    moment().subtract(1, 'month').endOf('month')
                                                                ],

                                                            },
                                                            locale: {
                                                                singleMonthView: true,
                                                                cancelLabel: 'Clear',
                                                                fromLabel: 'From',
                                                                toLabel: 'To',
                                                                customRangeLabel: 'Custom',
                                                                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                                                                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                                            },

                                                        }, function (start, end, label) {
                                                            //console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                                                        });


                                                        $('#picker').on('apply.daterangepicker', function (ev, picker) {

                                                            $('#hdnCustomStart').val(picker.startDate.format('YYYY-MM-DD') + " 12:00:00 am");
                                                            $('#hdnCustomEnd').val(picker.endDate.format('YYYY-MM-DD') + " 11:59:59 pm");
                                                            $('#picker').html(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                                                            $('#hdnDateRange').val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
                                                            var table = $('#tblActivation').DataTable();
                                                            table.destroy();
                                                            table.clear();
                                                            loadtable();
                                                        });

                                                        loadtable();
                                                    });
                                                    $('#picker').on('cancel.daterangepicker', function (ev, picker) {
                                                        $('#picker').html('<i class="material-icons">date_range</i>');

                                                        $('#hdnDateRange').val(moment().subtract(29, 'days').format('YYYY-MM-DD') + ' to ' + moment().format('YYYY-MM-DD'));
                                                        $('#hdnCustomStart').val(startdate);
                                                        $('#hdnCustomEnd').val(enddate);
                                                        $('#picker').html(moment().subtract(29, 'days').format('DD/MM/YYYY') + ' - ' + moment().format('DD/MM/YYYY'));
                                                        $('#picker').data('daterangepicker').setStartDate(moment().subtract(29, 'days'));
                                                        $('#picker').data('daterangepicker').setEndDate(moment());
                                                        $('#picker').data('daterangepicker').hideCalendars();
                                                        $('#hdnArchieveFlag').val('N');
                                                        $('#hdnDate').val('New');
                                                        $('#picker').show();
                                                        var table = $('#tblActivation').DataTable();
                                                        table.destroy();
                                                        table.clear();
                                                        loadtable();
                                                    });



                                                    function loadtable() {
                                                        $('#tblActivation').DataTable({
                                                            dom: 'Bfrtip',
                                                            buttons: [
                                                                {
                                                                    extend: 'excelHtml5',
                                                                    action: generateajaxcall,
                                                                    text: 'EXPORT IN XLS',
                                                                },
                                                            ],
                                                            "searching": false,
                                                            "responsive": true,
                                                            "processing": true,
                                                            "serverSide": true,
                                                            "bLengthChange": false,
                                                            "ordering": false,
                                                            ajax: {
                                                                url: "ajax/HelpMeReport" + extension,
                                                                cache: false,
                                                                type: "POST",
                                                                data: {
                                                                    offset: offset,
                                                                    Country: $('#hdnCountry').val(),
                                                                    FilterGroupId: $('#hdnGroupId').val(),
                                                                    PackageType: $('#ddlPackType option:selected').val(),
                                                                    CustomStartDate: $('#hdnCustomStart').val(),
                                                                    CustomEndDate: $('#hdnCustomEnd').val(),
                                                                    SubscriptionType: "",
                                                                },
                                                            },
                                                            "initComplete": function (settings, json) {
                                                                var info = this.api().page.info();
                                                                $('#hdnTotalRecords').val(info.recordsTotal);

                                                            },

                                                            columns: [
                                                                {data: "UserName"},
                                                                {data: "UserEmail"},
                                                                {data: "UserPhone"},
                                                                {data: "SubscriptionFrom"},
                                                                {data: "SubscriptionTo"},
                                                                {data: "SubscriptionType"},
                                                                {data: "AdminGroupName"},
                                                                {data: "PackageName"},
                                                                {data: "ActivatedDate"},
                                                                {data: "CreatedDate"},
                                                                {data: "Days"},
                                                                {data: "MemberType"},
                                                                {data: "LinkedMember"},
                                                                {data: "MonthlySubscription"},
                                                                {data: "SaleBy"},
                                                                {data: "ClientGroup"},
                                                                {data: "CampaignSource"},

                                                                {data: "PromoCode"},
                                                                {data: "IdNumber"},
                                                                {data: "Address1"},
                                                                {data: "Address2"},
                                                            ],

                                                        });


                                                    }
                                                    function ApplyFilters() {
                                                        $('#hdnGroupId').val($('#ddlGroup').val());
                                                        $('#hdnCountry').val($('#ddlCountry').val());


                                                        var table = $('#tblActivation').DataTable();
                                                        table.destroy();
                                                        table.clear();
                                                        loadtable();


                                                    }
                                                    function ClearFilters() {
                                                        $("select").val(null).trigger('change');
                                                        $('#hdnCountry').val('');
                                                        $('#hdnGroupId').val('');

                                                        var table = $('#tblActivation').DataTable();
                                                        table.destroy();
                                                        table.clear();
                                                        loadtable();

                                                    }

                                                    function FilterTypeChange(element) {
                                                        if (element.checked == true) {
                                                            $('#hdnFilterType').val('Month');
                                                        } else {
                                                            $('#hdnFilterType').val('Day');
                                                        }
                                                        loadtable();

                                                    }


                                                    var th = 0;
                                                    var j = 1;
                                                    var today = moment();
                                                    var curDate = today.format('YYYY-MM-DD');
                                                    var curDateTime = today.format('YYYY-MM-DD HH:mm:ss');
                                                    var excel, formatContent, textStyle, isstringStyle, dStyle, zeroStyle;

                                                    function generateajaxcall() {

                                                        var TotalLength = parseInt($('#hdnTotalRecords').val());
                                                        var startLength = parseInt($('#hdnStartCount').val());
                                                        var limit = parseInt($('#hdnLimitExport').val());
                                                        if (TotalLength > 500) {
                                                            var perc = parseInt((limit / TotalLength * 100));
                                                            if (perc > 100) {
                                                                var percentage = "100%";
                                                            } else {
                                                                var percentage = perc + "%";
                                                            }
                                                        } else {
                                                            var percentage = "100%";
                                                        }
                                                        if ($('#hdnExportClicked').val() == 'N') {
                                                            th = 0;
                                                            j = 1;
                                                            excel = $JExcel.new("Arial dark 10 #333333");
                                                            excel.set({sheet: 0, value: "HelpMe Activation Report"});


                                                            var headers = ["SL.No", "UserName", "UserEmail", "UserPhone", "SubscriptionFrom", "SubscriptionTo", "SubscriptionType", "AdminGroupName", "PackageName", "ActivatedDate", "CreatedDate", "Days", "MemberType", "LinkedMember", "MonthlySubscription", "SaleBy", "ClientGroup", "CampaignSource", "PromoCode", "IdNumber", "Address1", "Address2"];

                                                            var formatHeader = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 10 #333333 B",
                                                                align: "C C"});
                                                            formatContent = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C"});
                                                            zeroStyle = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C",
                                                                format: '0'});
                                                            dStyle = excel.addStyle({
                                                                align: "R",
                                                                format: "yyyy.mm.dd hh:mm:ss",
                                                                font: "#00AA00"}
                                                            );
                                                            textStyle = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C",
                                                                format: "@"});
                                                            isstringStyle = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C",
                                                                format: "@",
                                                                isstring: true
                                                            });
                                                            for (var i = 0; i < headers.length; i++) {
                                                                excel.set(0, i, 0, headers[i], formatHeader);
                                                                excel.set(0, i, undefined, 40);
                                                            }
                                                        }
                                                        $('#divProgress').attr('style', 'width:' + percentage);
                                                        $("#spnProgress").text(percentage);
                                                        $(".overlay").show();
                                                        $("#progress").show();
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "ajax/HelpMeReport" + extension,
                                                            data: {
                                                                offset: offset,
                                                                PackageType: $('#ddlPackType').val(),
                                                                CustomStartDate: $('#hdnCustomStart').val(),
                                                                CustomEndDate: $('#hdnCustomEnd').val(),
                                                                SubscriptionType: "",
                                                                length: "500",
                                                                start: startLength,
                                                                Country: $('#hdnCountry').val(),
                                                                FilterGroupId: $('#hdnGroupId').val(),
                                                            },
                                                            success: function (data) {
                                                                $('#hdnExportClicked').val('Y');
                                                                var parsedData = JSON.parse(data);
                                                                dataArray = [];
                                                                dataArray = parsedData.data;
                                                                if (dataArray.length > 0) {
                                                                    th++;
                                                                    console.log(th);
                                                                    for (var i = 0; i < dataArray.length; i++) {

                                                                        excel.set(0, 0, j, j, formatContent);

                                                                        excel.set(0, 1, j, removeSpecial(dataArray[i]['UserName']), textStyle);
                                                                        excel.set(0, 2, j, dataArray[i]['UserEmail'], textStyle);
                                                                        excel.set(0, 3, j, dataArray[i]['UserPhone'], isstringStyle);
                                                                        excel.set(0, 4, j, dataArray[i]['SubscriptionFrom'], textStyle);
                                                                        excel.set(0, 5, j, dataArray[i]['SubscriptionTo'], textStyle);
                                                                        excel.set(0, 6, j, dataArray[i]['SubscriptionType'], textStyle);
                                                                        excel.set(0, 7, j, removeSpecial(dataArray[i]['AdminGroupName']), textStyle);
                                                                        excel.set(0, 8, j, dataArray[i]['PackageName'], textStyle);
                                                                        excel.set(0, 9, j, dataArray[i]['ActivatedDate'], textStyle);
                                                                        excel.set(0, 10, j, dataArray[i]['CreatedDate'], textStyle);
                                                                        excel.set(0, 11, j, dataArray[i]['Days'], textStyle);
                                                                        excel.set(0, 12, j, dataArray[i]['MemberType'], isstringStyle);
                                                                        excel.set(0, 13, j, dataArray[i]['LinkedMember'], isstringStyle);
                                                                        excel.set(0, 14, j, dataArray[i]['MonthlySubscription'], isstringStyle);
                                                                        excel.set(0, 15, j, dataArray[i]['SaleBy'], isstringStyle);
                                                                        excel.set(0, 16, j, dataArray[i]['ClientGroup'], textStyle);
                                                                        excel.set(0, 17, j, dataArray[i]['CampaignSource'], textStyle);
                                                                        excel.set(0, 18, j, dataArray[i]['PromoCode'], textStyle);
                                                                        excel.set(0, 19, j, dataArray[i]['IdNumber'], textStyle);
                                                                        excel.set(0, 20, j, dataArray[i]['Address1'], textStyle);
                                                                        excel.set(0, 21, j, dataArray[i]['Address2'], textStyle);

                                                                        j++;
                                                                    }
                                                                }
                                                                if (TotalLength > limit) {
                                                                    $('#hdnStartCount').val(parseInt($('#hdnStartCount').val()) + 10);
                                                                    $('#hdnLimitExport').val(parseInt($('#hdnLimitExport').val()) + 500);
                                                                    setTimeout(function () {
                                                                        generateajaxcall();
                                                                    }, 3000);
                                                                } else {
                                                                    $('#hdnStartCount').val("0");
                                                                    $('#hdnLimitExport').val("500");

                                                                    excel.generate("HelpMe-Users " + $('#hdnDateRange').val() + ".xlsx");
                                                                    $(".overlay").hide();
                                                                    $("#progress").hide();
                                                                    $('#hdnExportClicked').val('N');
                                                                }

                                                            }

                                                        });
                                                    }



        </script>
    </body>
</html>