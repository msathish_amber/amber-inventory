<?php
include("config/config.php");
if (empty($_SESSION['Amber_Inventory_UserToken']) && $_SESSION['Amber_Inventory_Screen_OpenApi'] != 'Y') {
    header("Location: index" . $extension);
}
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>Amber :: Inventory</title>

        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
        <!-- BEGIN: VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="vendors/data-tables/css/select.dataTables.min.css">
        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/themes/horizontal-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="css/layouts/style-horizontal.css">
        <link rel="stylesheet" type="text/css" href="css/pages/dashboard.css">
        <link rel="stylesheet" type="text/css" href="css/pages/data-tables.css">

        <!-- END: Page Level CSS-->
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="css/custom/custom.css?version=<?php echo $cssVersion; ?>">
        <!-- END: Custom CSS-->
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
        <script src="js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="vendors/sparkline/jquery.sparkline.min.js"></script>
        <script src="vendors/chartjs/chart.min.js"></script>
        <script src="vendors/jquery-jvectormap/jquery-jvectormap.min.js"></script>
        <script src="vendors/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN THEME  JS-->
        <script src="js/plugins.js" type="text/javascript"></script>

        <link href="css/custom/MonthPicker.css" rel="stylesheet" type="text/css"/>
        <style>
            #chart-dashboard .card{
                min-height:600px;
            }
            .btnsmall{
                width:20%;
            }

            .select-wrapper>ul{
                max-height: 200px !important;
            }
            .overlay {
                background: #e9e9e9;
                display: none;
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                opacity: 0.5;
                z-index:9999;
            }
            #progress{
                display:none;
            }
        </style>
    </head>
    <!-- END: Head-->
    <body class="horizontal-layout page-header-light horizontal-menu 2-columns" data-open="click" data-menu="horizontal-menu" data-col="2-columns">
        <!-- BEGIN: Header-->
        <?php include("header.php"); ?>
        <!-- END: Header-->

        <!-- BEGIN: Page Main-->
        <div id="main">
            <div class="row">
                <div class="col s12">
                    <div class="container">
                        <div id="chart-dashboard">
                            <div class="row">
                                <div class="overlay">
                                </div>
                                <div class="div col s5 offset-s3" id="progress" style=" position: fixed; top: 50%;z-index: 99999; background: white;">
                                    <span>    Downloading Progress</span><span id="spnProgress" style="float:right;">0%</span>
                                    <div class="progress">

                                        <div class="determinate" id="divProgress" ></div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l6">
                                    <h5 class="breadcrumbs-title">Open API Usage Report</h5>

                                </div>
                                <div class="col s12 m6 l6 right-align-md" style="margin-top: 10px;">

                                </div>

                                <div class="col s12 m9 ">
                                    <div class="card animate fadeUp" id="divRenewalReport" style="padding-bottom: 2%;min-height: auto;">

                                    </div>
                                </div>
                                <div class="col s12 m3 l3 pr-0 hide-on-med-and-down animate fadeLeft">
                                    <div class="card" style="min-height: auto;">

                                        <div class="card-content">
                                            <span class="card-title">Filters</span>
                                            <hr class="p-0 mb-10">
                                            <div class="col s12" style="padding-bottom:15px;">
                                                <label>
                                                    <input class="with-gap" id="rbtnGroup" value="Group" name="rbtnFilter" type="radio" checked />
                                                    <span>Group </span>
                                                </label>
                                                <label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                                <label>
                                                    <input class="with-gap" id="rbtnCountry" value="Country" name="rbtnFilter" type="radio"  />
                                                    <span>Country </span>
                                                </label>
                                            </div>
                                            <div class="col s12" id="divGroup">
                                                <label>Filter By Group</label>
                                                <select multiple id="ddlGroup" >

                                                    <?php
                                                    $usersGroupUrl = $service_domain . "inventory/listgroups";

                                                    $usersGroupData = getData($usersGroupUrl, $commonPostArray);
                                                    $usersGroupJson = json_decode($usersGroupData);
                                                    $usersGroupArray = $usersGroupJson->Items;
                                                    $usersGroupCount = count($usersGroupArray);
                                                    ?>

                                                    <?php
                                                    for ($ci = 0; $ci < $usersGroupCount; $ci++) {
                                                        ?>
                                                        <option value="<?php echo $usersGroupArray[$ci]->GroupId; ?>"><?php echo $usersGroupArray[$ci]->GroupName; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                            <div class="col s12" id="divCountry">
                                                <label>Filter By Country</label>
                                                <select multiple id="ddlCountry" >

                                                    <?php
                                                    $countryUrl = $service_domain . "inventory/getcountrylist";

                                                    $countryData = getData($countryUrl, $commonPostArray);
                                                    $countryJson = json_decode($countryData);
                                                    $countryArray = $countryJson->Items;
                                                    $countryCount = count($countryArray);
                                                    ?>

                                                    <?php
                                                    for ($ci = 0; $ci < $countryCount; $ci++) {
                                                        ?>
                                                        <option value="<?php echo $countryArray[$ci]->Country; ?>"><?php echo $countryArray[$ci]->Country; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                            </div>




                                            <div class="input-field col s12">
                                                <i class="material-icons prefix">date_range</i>
                                                <input  id="txtMonthPicker" class="monthpicker" type="text" value="<?php echo date('Y-m', strtotime('-1 months')); ?>">
                                                <label for="txtMonthPicker">Filter By Month</label>
                                            </div>
                                            <center> <a class="mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan" style="background: -webkit-linear-gradient(45deg, #0288d1, #26c6da) !important;" onclick="ApplyFilters();">Apply</a>
                                                <a class="mb-6 btn waves-effect waves-light gradient-45deg-red-pink" onclick="ClearFilters();">Clear</a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END: Page Main-->

        <!-- BEGIN: Footer-->
        <?php include("footer.php"); ?>
        <!-- END: Footer-->
        <input type="hidden" id="hdnUsageYear" name="hdnUsageYear" value="">
        <input type="hidden" id="hdnUsageMonth" name="hdnUsageMonth" value="">
        <input type="hidden" id="hdnGroupId" name="hdnGroupId" value="">
        <input type="hidden" id="hdnCountry" name="hdnCountry" value="">

        <input type="hidden" id="hdnTableLoaded" name="hdnTableLoaded" value="No">
        <input type="hidden" id="hdnDateRange" name="hdnDateRange" value="">
        <input type="hidden" id="hdnTotalRecords" name="hdnTotalRecords" value="">
        <input type="hidden" id="hdnStartCount" name="hdnStartCount" value="0">
        <input type="hidden" id="hdnExportClicked" name="hdnExportClicked" value="N">
        <input type="hidden" id="hdnLimitExport" name="hdnLimitExport" value="500">
        <!-- BEGIN VENDOR JS-->

        <script src="js/custom/custom-script.js?version=<?php echo $jsVersion; ?>" type="text/javascript"></script>
        <!-- END THEME  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <!-- END PAGE LEVEL JS-->


        <script src="vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
        <script src="js/scripts/data-tables.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script src="//cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="//cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="js/moment.js" type="text/javascript"></script>  
        <script src="js/moment-timezone-with-data.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js" type="text/javascript"></script>

        <script src="css/custom/MonthPicker.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jszip.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/FileSaver.js?version=<?php echo $jsVersion; ?>"></script>
        <script type="text/javascript" src="js/myexcel.js?version=<?php echo $jsVersion; ?>"></script>
        <script>

                                                    $("input[type='radio']").click(function () {
                                                        var radioValue = $("input[name='rbtnFilter']:checked").val();
                                                        if (radioValue == 'Group') {
                                                            $('#divGroup').show();
                                                            $('#divCountry').hide();
                                                        } else {
                                                            $('#divGroup').hide();
                                                            $('#divCountry').show();
                                                        }
                                                        ClearFilters();

                                                    });
                                                    $('#divCountry').hide();

                                                    var initMonth = moment().subtract(1, 'month').startOf('month').format('MM');
                                                    var initYear = moment().subtract(1, 'month').startOf('month').format('YYYY');
                                                    $('#hdnUsageMonth').val(initMonth);
                                                    $('#hdnUsageYear').val(initYear);
                                                    $('#hdnDateRange').val(moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD') + ' to ' + moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD'));
                                                    $(document).ready(function () {
                                                        $('select').select2({width: "100%"});

                                                    });
                                                    var th = 0;
                                                    var j = 1;
                                                    var today = moment();
                                                    var curDate = today.format('YYYY-MM-DD');
                                                    var curDateTime = today.format('YYYY-MM-DD HH:mm:ss');

                                                    var excel, formatContent, textStyle, isstringStyle, dStyle, zeroStyle;

                                                    function generateajaxcall() {

                                                        var TotalLength = parseInt($('#hdnTotalRecords').val());
                                                        var startLength = parseInt($('#hdnStartCount').val());
                                                        var limit = parseInt($('#hdnLimitExport').val());
                                                        if (TotalLength > 500) {
                                                            var perc = parseInt((limit / TotalLength * 100));
                                                            if (perc > 100) {
                                                                var percentage = "100%";
                                                            } else {
                                                                var percentage = perc + "%";
                                                            }
                                                        } else {
                                                            var percentage = "100%";
                                                        }
                                                        $('#divProgress').attr('style', 'width:' + percentage);
                                                        $("#spnProgress").text(percentage);
                                                        $(".overlay").show();
                                                        $("#progress").show();
                                                        if ($('#hdnExportClicked').val() == 'N') {
                                                            th = 0;
                                                            j = 1;

                                                            excel = $JExcel.new("Arial dark 10 #333333");
                                                            excel.set({sheet: 0, value: "Open Api Usage Report"});
                                                            var headers = ["SL.No", "CompanyName", "Email", "Country", "DeviceCount", "APIHits", "APIKey", "Month", "MonthName", "Year","GroupName"];
                                                            var formatHeader = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 10 #333333 B",
                                                                align: "C C"});

                                                            formatContent = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C"});
                                                            zeroStyle = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C",
                                                                format: '0'});
                                                            dStyle = excel.addStyle({
                                                                align: "R",
                                                                format: "yyyy.mm.dd hh:mm:ss",
                                                                font: "#00AA00"}
                                                            );
                                                            textStyle = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C",
                                                                format: "@"});
                                                            isstringStyle = excel.addStyle({
                                                                border: "none,none,none,thin #333333",
                                                                font: "Arial 11 #333333",
                                                                align: "C C",
                                                                format: "@",
                                                                isstring: true
                                                            });

                                                            for (var i = 0; i < headers.length; i++) {
                                                                excel.set(0, i, 0, headers[i], formatHeader);
                                                                excel.set(0, i, undefined, 40);
                                                            }


                                                        }
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "ajax/OpenApiReport" + extension,
                                                            data: {
                                                                offset: offset,
                                                                Country: $('#hdnCountry').val(),
                                                                FilterGroupId: $('#hdnGroupId').val(),
                                                                UsageYear: $('#hdnUsageYear').val(),
                                                                UsageMonth: $('#hdnUsageMonth').val(),
                                                                length: "500",
                                                                start: startLength,
                                                            },
                                                            success: function (data) {
                                                                $('#hdnExportClicked').val('Y');
                                                                var parsedData = JSON.parse(data);
                                                                dataArray = [];
                                                                dataArray = parsedData.data;

                                                                if (dataArray.length > 0) {
                                                                    th++;

                                                                    for (var i = 0; i < dataArray.length; i++) {

                                                                        excel.set(0, 0, j, j, formatContent);
                                                                        excel.set(0, 1, j, removeSpecial(dataArray[i]['CompanyName']), textStyle);
                                                                        excel.set(0, 2, j, dataArray[i]['Email'], textStyle);
                                                                        excel.set(0, 3, j, removeSpecial(dataArray[i]['Country']), textStyle);
                                                                        excel.set(0, 4, j, removeSpecial(dataArray[i]['DeviceCount']), textStyle);
                                                                        excel.set(0, 5, j, removeSpecial(dataArray[i]['MonthlyUsage']), textStyle);
                                                                        var api = dataArray[i]['APIkey'];
                                                                        var returnstr = api.replace(/\w(?=(?:\W*\w){6})/g, "*");
                                                                        excel.set(0, 6, j, removeSpecial(returnstr), textStyle);
                                                                        excel.set(0, 7, j, removeSpecial(dataArray[i]['Month']), textStyle);
                                                                        excel.set(0, 8, j, removeSpecial(dataArray[i]['MonthName']), textStyle);
                                                                        excel.set(0, 9, j, removeSpecial(dataArray[i]['Year']), textStyle);
                                                                         excel.set(0, 10, j, removeSpecial(dataArray[i]['GroupName']), textStyle);
                                                                        j++;
                                                                    }

                                                                }
                                                                if (TotalLength > limit) {

                                                                    $('#hdnStartCount').val(parseInt($('#hdnStartCount').val()) + 10);
                                                                    $('#hdnLimitExport').val(parseInt($('#hdnLimitExport').val()) + 500);
                                                                    setTimeout(function () {
                                                                        generateajaxcall();
                                                                    }, 3000);

                                                                } else {
                                                                    $('#hdnStartCount').val("0");
                                                                    $('#hdnLimitExport').val("500");

                                                                    excel.generate("Open-Api-Usage" + curDateTime + ".xlsx");
                                                                    $(".overlay").hide();
                                                                    $("#progress").hide();
                                                                    $('#hdnExportClicked').val('N');
                                                                }

                                                            }

                                                        });
                                                    }

                                                    function getRenewalGraph() {

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "ajax/OpenApiReportChart" + extension,
                                                            data: {
                                                                offset: offset,
                                                                Country: $('#hdnCountry').val(),
                                                                FilterGroupId: $('#hdnGroupId').val(),
                                                                UsageYear: $('#hdnUsageYear').val(),
                                                                UsageMonth: $('#hdnUsageMonth').val(),
                                                            },
                                                            success: function (data) {

                                                                var results = data.split("~~");
                                                                if ($.trim(results[0]) == 'Y') {
                                                                    $('#divRenewalReport').html(results[1]);

                                                                    var parsedGroup = $.parseJSON(results[2]);
                                                                    var parsedGroupCount = $.parseJSON(results[3]);

                                                                    /*
                                                                     Doughnut Chart Widget
                                                                     */

                                                                    var doughnutSalesChartCTX = $("#doughnut-chart");
                                                                    var browserStatsChartOptions = {
                                                                        cutoutPercentage: 70,
                                                                        legend: {
                                                                            display: false
                                                                        }
                                                                    };
                                                                    var doughnutSalesChartData = {
                                                                        labels: parsedGroup,
                                                                        datasets: [
                                                                            {
                                                                                label: "Activated",
                                                                                data: parsedGroupCount,
                                                                                backgroundColor: ["#5946bf", "#bb489d", "#2b791d", "#d67714", "#e826d9", "#f7104f", "#d67714", "#e826d9", "#f7104f", "#5946bf", "#bb489d", "#2b791d"]
                                                                            }
                                                                        ]
                                                                    };
                                                                    var doughnutSalesChartConfig = {
                                                                        type: "doughnut",
                                                                        options: browserStatsChartOptions,
                                                                        data: doughnutSalesChartData
                                                                    };
                                                                    /*
                                                                     Monthly Revenue : Trending Bar Chart
                                                                     */

                                                                    var monthlyRevenueChartCTX = $("#trending-bar-chart");
                                                                    var monthlyRevenueChartOptions = {
                                                                        responsive: true,
                                                                        // maintainAspectRatio: false,
                                                                        legend: {
                                                                            display: false
                                                                        },
                                                                        hover: {
                                                                            mode: "label",
                                                                        },
                                                                        "animation": {
                                                                            "duration": 1,
                                                                            "onComplete": function () {
                                                                                var chartInstance = this.chart,
                                                                                        ctx = chartInstance.ctx;
                                                                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                                                                ctx.textAlign = 'center';
                                                                                ctx.textBaseline = 'bottom';
                                                                                ctx.fillStyle = 'white';
                                                                                this.data.datasets.forEach(function (dataset, i) {
                                                                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                                                                    meta.data.forEach(function (bar, index) {
                                                                                        var data = dataset.data[index];
                                                                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                                                                    });
                                                                                });
                                                                            }
                                                                        },
                                                                        scales: {
                                                                            xAxes: [
                                                                                {
                                                                                    display: true,
                                                                                    gridLines: {
                                                                                        display: false
                                                                                    },
                                                                                    ticks: {
                                                                                        autoSkip: false,
                                                                                        fontColor: 'white'
                                                                                    }
                                                                                }
                                                                            ],
                                                                            yAxes: [
                                                                                {
                                                                                    display: true,
                                                                                    fontColor: "#fff",
                                                                                    gridLines: {
                                                                                        display: false
                                                                                    },
                                                                                    ticks: {
                                                                                        beginAtZero: true,
                                                                                        fontColor: 'white'
                                                                                    }
                                                                                }
                                                                            ]
                                                                        },
                                                                        tooltips: {
                                                                            titleFontSize: 0,
                                                                            callbacks: {
                                                                                label: function (tooltipItem, data) {
                                                                                    return tooltipItem.yLabel;
                                                                                }
                                                                            }
                                                                        }
                                                                    };
                                                                    var monthlyRevenueChartData = {
                                                                        labels: parsedGroup,
                                                                        datasets: [
                                                                            {
                                                                                label: "Activated",
                                                                                data: parsedGroupCount,
                                                                                backgroundColor: "#46BFBD",
                                                                                hoverBackgroundColor: "#009688"
                                                                            }
                                                                        ]
                                                                    };
                                                                    var nReloads1 = 0;
                                                                    var min1 = 1;
                                                                    var max1 = 10;
                                                                    var monthlyRevenueChart;
                                                                    var monthlyRevenueChartConfig = {
                                                                        type: "bar",
                                                                        options: monthlyRevenueChartOptions,
                                                                        data: monthlyRevenueChartData
                                                                    };
                                                                    monthlyRevenueChart = new Chart(monthlyRevenueChartCTX, monthlyRevenueChartConfig);


                                                                }

                                                            }
                                                        });
                                                    }

                                                    getRenewalGraph();
                                                    function LoadDataTable() {
                                                        $('#hdnTableLoaded').val('Yes');
                                                        $('.activator').attr("onclick", "").unbind("click");
                                                        $('#tblOpenApi').DataTable({
                                                            dom: 'Bfrtip',
                                                            buttons: [
                                                                {
                                                                    extend: 'excelHtml5',
                                                                    action: generateajaxcall,
                                                                    text: 'EXPORT IN XLS',
                                                                },
                                                            ],
                                                            "searching": false,
                                                            "responsive": true,
                                                            "processing": true,
                                                            "serverSide": true,
                                                            "bLengthChange": false,
                                                            "ordering": false,
                                                            ajax: {
                                                                url: "ajax/OpenApiReport" + extension,
                                                                cache: false,
                                                                data: {
                                                                    offset: offset,
                                                                    Country: $('#hdnCountry').val(),
                                                                    FilterGroupId: $('#hdnGroupId').val(),
                                                                    UsageYear: $('#hdnUsageYear').val(),
                                                                    UsageMonth: $('#hdnUsageMonth').val(),
                                                                },
                                                                type: "POST"

                                                            },
                                                            "initComplete": function (settings, json) {
                                                                var info = this.api().page.info();
                                                                $('#hdnTotalRecords').val(info.recordsTotal);

                                                            },
                                                            columns: [
                                                                {data: "CompanyName"},
                                                                {data: "Email"},
                                                                {data: "Country"},
                                                                {data: "DeviceCount"},
                                                                {data: "MonthlyUsage"},

                                                                {
                                                                    "mData": null,
                                                                    "bSortable": false,
                                                                    "mRender": function (data, type, full) {
                                                                        var api = data.APIkey;

                                                                        var returnstr = api.replace(/\w(?=(?:\W*\w){6})/g, "*");

                                                                        return returnstr;

                                                                    }
                                                                },

                                                                {data: "Month"},
                                                                {data: "MonthName"},
                                                                {data: "Year"},
                                                                {data:"GroupName"}
                                                            ],
                                                        });
                                                    }
                                                    function ApplyFilters() {

                                                        if ($('#txtMonthPicker').val() == '') {
                                                            $('#txtMonthPicker').val(moment().subtract(1, 'month').format('YYYY-MM'));
                                                        }
                                                        var UsageMonth = moment($('#txtMonthPicker').val() + '-01').startOf('month').format('MM');

                                                        var UsageYear = moment($('#txtMonthPicker').val() + '-01').startOf('month').format('YYYY');

                                                        $('#hdnUsageMonth').val(UsageMonth);
                                                        $('#hdnUsageYear').val(UsageYear);
                                                        $('#hdnGroupId').val($('#ddlGroup').val());
                                                        $('#hdnCountry').val($('#ddlCountry').val());

                                                        getRenewalGraph();
                                                        if ($('#hdnTableLoaded').val() == 'Yes') {
                                                            var table = $('#tblOpenApi').DataTable();
                                                            table.destroy();
                                                            table.clear();
                                                        }
                                                        $('.activator').attr("onclick", "LoadDataTable()").bind("click");
                                                    }
                                                    function ClearFilters() {
                                                        $("select").val(null).trigger('change');
                                                        $('#hdnCountry').val('');
                                                        $('#hdnGroupId').val('');


                                                        getRenewalGraph();
                                                        if ($('#hdnTableLoaded').val() == 'Yes') {
                                                            var table = $('#tblOpenApi').DataTable();
                                                            table.destroy();
                                                            table.clear();
                                                        }
                                                        $('.activator').attr("onclick", "LoadDataTable()").bind("click");
                                                    }
                                                    var oldExportAction = function (self, e, dt, button, config) {
                                                        if (button[0].className.indexOf('buttons-copy') >= 0) {
                                                            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                                                        } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                                                            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                                                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                                                                    $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                                                        } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                                                            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                                                                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                                                                    $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                                                        } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                                                            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                                                                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                                                                    $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                                                        } else if (button[0].className.indexOf('buttons-print') >= 0) {
                                                            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                                                        }
                                                    };

                                                    var newExportAction = function (e, dt, button, config) {
                                                        var self = this;
                                                        var oldStart = dt.settings()[0]._iDisplayStart;

                                                        dt.one('preXhr', function (e, s, data) {
                                                            // Just this once, load all data from the server...
                                                            data.start = 0;
                                                            data.length = 2147483647;

                                                            dt.one('preDraw', function (e, settings) {
                                                                // Call the original action function 
                                                                oldExportAction(self, e, dt, button, config);

                                                                dt.one('preXhr', function (e, s, data) {
                                                                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                                                                    // Set the property to what it was before exporting.
                                                                    settings._iDisplayStart = oldStart;
                                                                    data.start = oldStart;
                                                                });

                                                                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                                                                setTimeout(dt.ajax.reload, 0);

                                                                // Prevent rendering of the full data to the DOM
                                                                return false;
                                                            });
                                                        });

                                                        // Requery the server with the new one-time export settings
                                                        dt.ajax.reload();

                                                    };
                                                    function FilterTypeChange(element) {
                                                        if (element.checked == true) {
                                                            $('#hdnFilterType').val('Month');
                                                        } else {
                                                            $('#hdnFilterType').val('Day');
                                                        }
                                                        getRenewalGraph();

                                                    }
        </script>



    </body>
</html>