<?php
$DASHBOARD = $_SESSION['Amber_Inventory_Screen_Dashboard'];
if ($DASHBOARD == 'Y') {
    $DASHBOARD = '';
} else {
    $DASHBOARD = 'hide';
}
$STOCKREPORT = $_SESSION['Amber_Inventory_Screen_StockReport'];
if ($STOCKREPORT == 'Y') {
    $STOCKREPORT = '';
} else {
    $STOCKREPORT = 'hide';
}
$SUBSCRIPTIONREPORT = $_SESSION['Amber_Inventory_Screen_SubscriptionReport'];
if ($SUBSCRIPTIONREPORT == 'Y') {
    $SUBSCRIPTIONREPORT = '';
} else {
    $SUBSCRIPTIONREPORT = 'hide';
}
if ($STOCKREPORT != '' && $SUBSCRIPTIONREPORT != '') {
    $Reports = 'hide';
}

$DEVICEACTIVATION = $_SESSION['Amber_Inventory_Screen_DeviceActivation'];
if ($DEVICEACTIVATION == 'Y') {
    $DEVICEACTIVATION = '';
} else {
    $DEVICEACTIVATION = 'hide';
}
$SHIELDACTIVATION = $_SESSION['Amber_Inventory_Screen_ShieldActivation'];
if ($SHIELDACTIVATION == 'Y') {
    $SHIELDACTIVATION = '';
} else {
    $SHIELDACTIVATION = 'hide';
}

if ($DEVICEACTIVATION != '' && $SHIELDACTIVATION != '') {
    $ActivationReports = 'hide';
}

$DEVICERENEWAL = $_SESSION['Amber_Inventory_Screen_DeviceRenewal'];
if ($DEVICERENEWAL == 'Y') {
    $DEVICERENEWAL = '';
} else {
    $DEVICERENEWAL = 'hide';
}
$SHIELDRENEWAL = $_SESSION['Amber_Inventory_Screen_ShieldRenewal'];
if ($SHIELDRENEWAL == 'Y') {
    $SHIELDRENEWAL = '';
} else {
    $SHIELDRENEWAL = 'hide';
}

if ($DEVICERENEWAL != '' && $SHIELDRENEWAL != '') {
    $RenewalReports = 'hide';
}

$MONTHLYBILLING = $_SESSION['Amber_Inventory_Screen_MonthlyBilling'];
if ($MONTHLYBILLING == 'Y') {
    $MONTHLYBILLING = '';
} else {
    $MONTHLYBILLING = 'hide';
}
$UNITSNOTWORKING = $_SESSION['Amber_Inventory_Screen_UnitNotWorking'];
if ($UNITSNOTWORKING == 'Y') {
    $UNITSNOTWORKING = '';
} else {
    $UNITSNOTWORKING = 'hide';
}

$OPENAPI = $_SESSION['Amber_Inventory_Screen_OpenApi'];
if ($OPENAPI == 'Y') {
    $OPENAPI = '';
} else {
    $OPENAPI = 'hide';
}

if ($MONTHLYBILLING != '' && $UNITSNOTWORKING != '' && $OPENAPI != '') {
    $BillingReports = 'hide';
}

$USERSVIEW = $_SESSION['Amber_Inventory_Screen_UserView'];
if ($USERSVIEW == 'Y') {
    $USERSVIEW = '';
} else {
    $USERSVIEW = 'hide';
}
$USERSCREATE = $_SESSION['Amber_Inventory_Screen_UserCreate'];
if ($USERSCREATE == 'Y') {
    $USERSCREATE = '';
} else {
    $USERSCREATE = 'hide';
}
$USERSEDIT = $_SESSION['Amber_Inventory_Screen_UserEdit'];
if ($USERSEDIT == 'Y') {
    $USERSEDIT = '';
} else {
    $USERSEDIT = 'hide';
}
$USERSDELETE = $_SESSION['Amber_Inventory_Screen_UserDelete'];
if ($USERSDELETE == 'Y') {
    $USERSDELETE = '';
} else {
    $USERSDELETE = 'hide';
}
$USERSRESET_PASSWORD = $_SESSION['Amber_Inventory_Screen_UserReset'];
if ($USERSRESET_PASSWORD == 'Y') {
    $USERSRESET_PASSWORD = '';
} else {
    $USERSRESET_PASSWORD = 'hide';
}

$HELPMEACTIVATION = $_SESSION['Amber_Inventory_Screen_HelpMeActivation'];
if ($HELPMEACTIVATION == 'Y') {
    $HELPMEACTIVATION = '';
} else {
    $HELPMEACTIVATION = 'hide';
}
$HELPMERENEWAL = $_SESSION['Amber_Inventory_Screen_HelpMeRenewal'];
if ($HELPMERENEWAL == 'Y') {
    $HELPMERENEWAL = '';
} else {
    $HELPMERENEWAL = 'hide';
}

$HELPMEUSERS = $_SESSION['Amber_Inventory_Screen_HelpMeUsers'];
if ($HELPMEUSERS == 'Y') {
    $HELPMEUSERS = '';
} else {
    $HELPMEUSERS = 'hide';
}
if ($HELPMEACTIVATION != '' && $HELPMERENEWAL != '' && $HELPMEUSERS != '') {
    $HelpMeReports = 'hide';
}


?>

<header class="page-topbar" id="header">
    <div class="navbar navbar-fixed"> 
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-light-blue-cyan">
            <div class="nav-wrapper">
                <ul class="left">
                    <li>
                        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index<?php echo $extension; ?>"><img src="images/logo/Amber_Connect_White.png" alt="materialize logo"><span class="logo-text hide-on-med-and-down"></span></a></h1>
                    </li>
                </ul>

                <ul class="navbar-list right">

                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
                    <li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search         </i></a></li>

                    <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><i class="material-icons avatar-status avatar-online">person<i></i></i></a></li>

                </ul>
                <!-- translation-button-->

                <div class="hide"><a href="maintenance<?php echo $extension; ?>" class="header-maintenance">Thursday December 12th, Amber connect will perform a planned annual server maintenance. The maintenance window will last from 11:00 to 21:00 UTC.</a></div>
                <!-- profile-dropdown-->
                <ul class="dropdown-content" id="profile-dropdown">
                    <li><a class="grey-text text-darken-1 btn-timer" href="javascript:;"><i class="material-icons">person_outline</i> Profile</a></li>

                    <li class="divider"></li>
                    <li><a class="grey-text text-darken-1 btn-timer" href="javascript:;"><i class="material-icons">lock_outline</i> Lock</a></li>
                    <li><a class="grey-text text-darken-1 " href="logout<?php echo $extension; ?>"><i class="material-icons">keyboard_tab</i> Logout</a></li>
                </ul>
            </div>

        </nav>
        <!-- BEGIN: Horizontal nav start-->
        <nav class="white hide-on-med-and-down" id="horizontal-nav">
            <div class="nav-wrapper">
                <ul class="left hide-on-med-and-down" id="ul-horizontal-nav" data-menu="menu-navigation">
                    <?php if ($DASHBOARD == '') { ?>
                        <li class="bold "><a class="waves-effect waves-cyan " href="dashboard<?php echo $extension; ?>"><i class="material-icons">dashboard</i><span class="menu-title" data-i18n="">Dashboard</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($Reports == '') { ?>
                        <li ><a class="dropdown-menu" href="#" data-target="Cards"><i class="material-icons">cast</i><span>Reports<i class="material-icons right">keyboard_arrow_down</i></span></a>
                            <ul class="dropdown-content dropdown-horizontal-list" id="Cards">       
                                <li data-menu="" class="<?php echo $STOCKREPORT; ?>"><a href="globalstockreport<?php echo $extension; ?>">Stock Report</a>
                                </li>
                                <li data-menu="" class="<?php echo $SUBSCRIPTIONREPORT; ?>"><a href="subscriptionforecasting<?php echo $extension; ?>">Subscription Expiry Forecasting</a>
                                </li>

                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($ActivationReports == '') { ?>
                        <li><a class="dropdown-menu" href="#" data-target="Cards1"><i class="material-icons">cast</i><span>Activation Reports<i class="material-icons right">keyboard_arrow_down</i></span></a>
                            <ul class="dropdown-content dropdown-horizontal-list" id="Cards1"> 
                                <li data-menu="" class="<?php echo $DEVICEACTIVATION; ?>"><a href="deviceactivation<?php echo $extension; ?>">Device Activation Report</a>
                                </li>
                                <li data-menu="" class="<?php echo $SHIELDACTIVATION; ?>"><a href="shieldactivation<?php echo $extension; ?>">Shield Activation report</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($RenewalReports == '') { ?>
                        <li><a class="dropdown-menu" href="#" data-target="Cards2"><i class="material-icons">cast</i><span>Renewal Reports<i class="material-icons right">keyboard_arrow_down</i></span></a>
                            <ul class="dropdown-content dropdown-horizontal-list" id="Cards2"> 
                                <li data-menu="" class="<?php echo $DEVICERENEWAL; ?>"><a href="devicerenewal<?php echo $extension; ?>">Device Renewal Report&nbsp;&nbsp;&nbsp;</a>
                                </li>
                                <li data-menu="" class="<?php echo $SHIELDRENEWAL; ?>"><a href="shieldrenewal<?php echo $extension; ?>">Shield Renewal Report</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($BillingReports == '') { ?>
                        <li><a class="dropdown-menu" href="#" data-target="Cards3"><i class="material-icons">cast</i><span>Billing<i class="material-icons right">keyboard_arrow_down</i></span></a>
                            <ul class="dropdown-content dropdown-horizontal-list" id="Cards3"> 
                                <li data-menu="" class="<?php echo $MONTHLYBILLING; ?>"><a href="monthlyrenewal<?php echo $extension; ?>">Monthly Billable Subscriptions</a>
                                </li>
                                <li data-menu="" class="<?php echo $UNITSNOTWORKING; ?>"><a href="unitsrenewal<?php echo $extension; ?>">Units Not Working(Monthly Billable)</a>
                                </li>
                                <li data-menu="" class="<?php echo $OPENAPI; ?>"><a href="openapireport<?php echo $extension; ?>">Open API Usage Report</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                        <?php if ($HelpMeReports == '') { ?>
                        <li><a class="dropdown-menu" href="#" data-target="Cards4"><i class="material-icons">cast</i><span>HelpMe!<i class="material-icons right">keyboard_arrow_down</i></span></a>
                            <ul class="dropdown-content dropdown-horizontal-list" id="Cards4"> 
                                <li data-menu="" class="<?php echo $HELPMEACTIVATION; ?>"><a href="helpmeactivation<?php echo $extension; ?>">HelpMe! Activation Report</a>
                                </li>
                                <li data-menu="" class="<?php echo $HELPMERENEWAL; ?>"><a href="helpmerenewal<?php echo $extension; ?>">HelpMe! Renewal Report</a>
                                </li>
                                <li data-menu="" class="<?php echo $HELPMEUSERS; ?>"><a href="helpmeuser<?php echo $extension; ?>">HelpMe! Users</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($USERSVIEW == '') { ?>
                        <li class="bold" ><a class="waves-effect waves-cyan " href="users<?php echo $extension; ?>"><i class="material-icons">people</i><span class="menu-title" data-i18n="">Users</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <!-- END: Horizontal nav start-->
        </nav>
    </div>
</header>

<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-fixed hide-on-large-only">
    <div class="brand-sidebar sidenav-light"></div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed hide-on-large-only menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
        <li class="active bold"><a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="">Dashboard</span><span class="badge badge pill red float-right mr-10">3</span></a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li><a class="collapsible-body" href="dashboard-modern.html" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Modern</span></a>
                    </li>
                    <li class="active"><a class="collapsible-body active" href="dashboard-ecommerce.html" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>eCommerce</span></a>
                    </li>
                    <li><a class="collapsible-body" href="dashboard-analytics.html" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Analytics</span></a>
                    </li>
                </ul>
            </div>
        </li>

    </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->
